[[_TOC_]]

# PHP-CLI

# 1 - Conceitos básicos

Neste pequeno manual, que de forma alguma pretende ser uma espécie de guia definitivo, nós veremos algumas das principais características especiais do PHP-CLI e partiremos diretamente para os temas mais fundamentais para o aprendizado da linguagem PHP em si.

Antes de mais nada, é importante você saber que o PHP-CLI é essencialmente o mesmo PHP utilizado no desenvolvimento de aplicações para web. A única diferença é que ele é capaz de interagir diretamente com a linha de comandos, tornando-se uma poderosa alternativa para a criação de scripts e programas para uso no terminal e até no desktop.

Isso quer dizer que, apesar do nosso foco nas aplicações para a linha de comando, você pode acompanhar tranquilamente os tópicos deste manual como uma forma objetiva de compreender como o PHP funciona para, mais tarde, aplicar o que aprender no desenvolvimento para a web.

> **Importante!** Todo o conteúdo deste manual é dirigido para as plataformas *unix like* (especialmente os sistemas com base no GNU/Linux), mas tudo aqui pode ser perfeitamente estudado e exercitado em outros sistemas operacionais.

Antes de começarmos, porém, algumas convenções e conceitos preliminares:

**Prompt da linha de comando:**

Os comandos que precisarem ser executados na linha de comando aparecerão após este *prompt* genérico:

```
:~$
```

Embora o til (`~`) represente a pasta `home` do usuário, aqui ela significará apenas que o comando será executado na pasta em que estivermos trabalhando.

**Prompt do modo interativo**

Outro prompt que veremos muito por aqui será o do modo interativo:

```
php >
```

O modo interativo, como veremos mais adiante, será invocado com o comando:

```
:~$ php -a
```

Nele, nós poderemos testar e descobrir como alguns elementos da linguagem PHP funcionam.

**Exemplos e scripts**

Quando os exemplos aparecerem sem o prompt do modo interativo, significa que eles deverão ser testados em um script ou, em alguns casos, quando o exemplo for um script completo, ele também estará disponível no último apêndice deste manual.

**Instruções**

No PHP, cada linha de código terminada com `;` é uma *instrução*. Por analogia, se imaginarmos que estamos trabalhando no terminal, uma instrução pode ser comparada a um comando a ser executado ao teclarmos `Enter`.

**Scripts**

Quando escrevemos uma série de instruções a serem interpretadas e executadas em sequência pelo PHP, nós temos um *script*.

**Funções**

Dentro de um script em PHP, nós podemos criar funções, que são basicamente trechos de código que podem ser chamados para realizar alguma rotina específica e retornar valores. No PHP, as funções podem receber dados que irão parametrizar a sua execução. Esses dados são sempre passados para a função entre parêntesis. Então, mesmo que a função não espere parâmetros (ou **argumentos**), os parêntesis sempre deverão ser usados.

Por exemplo:

```
php > printf("Olá, mundo!");
Olá, mundo!
```

Aqui, `printf` é uma função do PHP que exige pelo menos um parâmetro entre os parêntesis (no caso, `"Olá, mundo!"`). Se uma função exige parâmetros e eles não forem passados, o PHP retornará um erro:

```
php > printf();
PHP Warning:  printf() expects at least 1 parameter, 0 given in php shell code on line 1
```

Outra funções podem não receber parâmetros, como a função `time`, por exemplo, mas elas devem ser chamadas com os parêntesis mesmo assim:

```
php > echo time();
1591363314
```

O PHP traz por padrão várias funções internas para as mais diversas finalidades. 

**Construtores de linguagem**

Os construtores de linguagem são entidades da linguagem PHP criados para se comportarem como funções, mas são tratados pelo interpretador de uma forma bem diferente.

De modo geral, os construtores são reconhecidos automaticamente pelo PHP como um elemento da própria linguagem sem que seja feita uma análise da estrutura da instrução, o que torna a sua execução mais rápida.

Outra característica dos construtores de linguagem que os difere das funções, é que eles **nem sempre** exigem parêntesis quando são chamados, como é o caso do `echo`:

```
php > echo "Olá, mundo!";
Olá, mundo!
php > echo ("Olá, mundo!");
Olá, mundo!
php > echo("Olá, mundo!");
Olá, mundo!
```

Mas, **preste atenção**: alguns construtores de linguagem, apesar de se parecerem com funções, dispensam os parêntesis. Alguns exigem, outros nem podem ser usados com parêntesis! Ou seja, cada caso é um caso e só o manual tem a sintaxe correta. O importante é saber quando estamos usando uma função e quando estamos usando um construtor de linguagem.
Agora que estamos falando a mesma língua, podemos continuar.

## 1.1 - Instalação e módulos sugeridos

A maioria das distribuições GNU/Linux fornece o PHP-CLI em seus repositórios oficiais com o nome de `php-cli`. O que costuma variar são as versões e os módulos instalados por padrão. No Debian Stable (atualmente na versão 10 Buster), por exemplo, o pacote `php-cli` instala a versão 7.3.14 e depende dos seguintes pacotes:

```
php7.3-cli
libedit2 (>= 2.11-20080614-4)
libmagic1
mime-support
7.3-common (= 7.3.14-1~deb10u1)
php7.3-json
php7.3-opcache
php7.3-readline
tzdata
ucf
libargon2-1 (>= 0~20171227)
libc6 (>= 2.27)
libpcre2-8-0 (>= 10.32)
libsodium23 (>= 1.0.14)
libssl1.1 (>= 1.1.0)
libxml2 (>= 2.8.0)
zlib1g (>= 1:1.1.4)
```

Além desses, nós sugerimos a instalação dos módulos do PHP forncecidos nos pacotes:

```
php-fpm
php-pdo
php-mysql
php-zip
php-gd
php-mbstring
php-curl
php-xml
php-pear
php-bcmath
```

> **Nota:** consulte a documentação da sua distribuição sobre como instalar o PHP-CLI e os módulos sugeridos.

## 1.2 - Opções da linha de comando

Com o PHP-CLI instalado, você terá o comando `php` à disposição para executar scripts, obter informações sobre o PHP, trabalhar no modo interativo e até iniciar um servidor web embutido para testar suas aplicações web, tudo a partir do comando `php`. São muitas as opções, e todas elas estão muito bem documentadas nos comandos `man php` e `php -h`. Contudo, é muito importante você se familiarizar com algumas delas, como veremos a seguir.

### 1.2.1 - Listando as opções

Para listar as opções de linha de comando do PHP, execute:

```
:~$ php -h
```

Para uma ajuda mais completa, nós também podemos consultar a página do manual:

```
:~$ man php
```

### 1.2.2 - Opções de execução de código

Para executar um script em PHP, basta executar o comando abaixo no terminal:

```
:~$ php <arquivo do script>
```

Mas, nós também podemos executar um código em PHP diretamente na linha de comando:

```
:~$ php <<< '<?php echo "Olá, mundo!\n"; ?>'
Olá, mundo!
:~$
```

Como o comando `php` espera um arquivo, nós precisamos utilizar um recurso do shell chamado ***here string*** (`<<<`), que equivale basicamente a enviar o código em PHP como uma saída do comando `echo` para a entrada do comando `php` via *pipe* (`|`):

```
:~$ echo '<?php echo "Olá, mundo!\n"; ?>' | php
Olá, mundo!
:~$
```

Repare que o código em PHP precisa estar entre as tags `<?php` e `?>`. Isso é um padrão nos códigos em PHP e serve essencialmente para "isolar" as instruções em PHP de outros tipos de conteúdo, como as tags HTML, por exemplo.

> **Nota:** PHP é um acrônimo recursivo para ***PHP: Hipertext Processor*** (PHP: Processador de Hipertexto). Isso significa que ele foi criado para mesclar-se com os elementos da linguagem de marcações de hipertexto HTML, gerando e controlando dinamicamente o que será exibido em um navegador da web.

Contudo, é possível evitar a *here string* e as tags do PHP com a opção `-r`:

```
php -r 'echo "Olá, mundo!\n";'
Olá, mundo!
:~$
```

Em ambos os casos, com a *here string* ou a opção `-r`, é importante observar que o código deve estar entre aspas simples para evitar as expansões do shell. Além disso, todas as instruções do código devem obrigatoriamente terminar com `;`.

### 1.2.3 - Obtendo informações sobre o PHP

Para obter uma lista completa e extensa de informações sobre as configurações da sua instalação do PHP, basta executar:

```
:~$ php -i
```

> **Uma dica:** como a lista apresentada é muito longa, nós podemos restringir a exibição aos termos que nos interessam com o utilitário do sistema `grep`:

```
:~$ php -i | grep -i memory
```

O que exibiria:

```
memory_limit => -1 => -1
Collecting memory statistics => No
opcache.memory_consumption => 128 => 128
opcache.preferred_memory_model => no value => no value
opcache.protect_memory => 0 => 0
```

> A sintaxe `grep -i` faz o `grep` ignorar diferenças entre caixa alta e baixa na busca.

### 1.2.4 - Módulos instalados

Também podemos ver uma lista dos módulos do PHP que estão instalados:

```
:~$ php -m
```

Os módulos servem para ampliar os recursos nativos do PHP com a inclusão de métodos que podem ser muito úteis -- ou até essenciais -- nos nossos projetos. Por exemplo, o PHP não traz funções nativas para algumas operações com caracteres *multibyte* (como os caracteres acentuados da língua portuguesa), o que pode ser resolvido com a instalação do módulo `php-mbstring`.

### 1.2.5 - Versão

Se quisermos saber qual versão do PHP está instalada, basta executar:

```
:~$ php -v
PHP 7.3.14-1~deb10u1 (cli) (built: Feb 16 2020 15:07:23) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.3.14, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.3.14-1~deb10u1, Copyright (c) 1999-2018, by Zend Technologies
```

## 1.3 - O modo interativo

Uma das opções mais interessantes do comando `php` é aquela que nos permite acessar o shell interativo da linguagem. Nele, nós podemos executar e testar códigos em PHP diretamente no terminal:

```
:~$ php -a
Interactive mode enabled

php > echo "Olá, mundo!\n";
Olá, mundo!
php > 
```

Para sair do modo interativo, nós utilizamos o comando `quit` ou executanis a instrução `exit`.

> **Nota:** também é possível sair do modo interativo com o atalho `Ctrl+C`.

Boa parte dos nossos exemplos sobre os elementos da linguagem PHP será demonstrada no modo interativo. Assim, você não precisará criar um arquivo para ver como os exemplos funcionam -- basta abrir um terminal e chamar o comando `php -a`. Além disso, você descobrirá que o modo interativo é uma excelente forma de aprender como as funções e instruções que estiver estudando funcionam.

## 1.4 - O servidor embutido

Outra opção poderosíssima do comando `php` é o servidor web embutido (*built in*).

```
:~$ php -S localhost:8000
```

Não é um servidor web completo, mas é o suficiente para o teste dos nossos projetos que envolvam saídas em HTML. Com o comando acima, basta abrirmos um navegador web e digitar `localhost:8000` na barra de endereços para termos acesso ao conteúdo da pasta onde o comando `php -S` foi executado.

Se não houver um arquivo `index.php` ou `index.html` nessa pasta, você verá a mensagem:

```
Not Found

The requested resource / was not found on this server.
```

Mesmo assim, você pode especificar o arquivo que deseja testar na barra de endereços do navegador:

```
localhost:8000/arquivo.php
``` 

Alternativamente, nós podemos especificar a pasta onde estão os arquivos do nosso projeto:

```
:~$ php -t ~/caminho/do/projeto -S localhost:8000
```

Além da visualização no navegador, o servidor embutido continuará monitorando tudo que acontecer durante a sua execução, exibindo mensagens no terminal que podem nos ajudar muito a encontrar eventuais erros.

Para terminar a execução do servidor, basta utilizar o atalho `Ctrl+C`.

## 1.5 - Como executar scripts

Como já vimos, os scripts em PHP podem ser executados no terminal pelo comando:

```
:~$ php <arquivo do script>
```

Mas isso só é necessário se o arquivo não for executável e não possuir uma linha indicando o interpretador de comandos. 

A linha do interpretador, ou "*shebang*", deve ser a primeira linha do script, e serve para iniciar uma sessão do interpretador de comandos (que, no nosso caso, é o `php`):

```
#!/usr/bin/env php
```

Para tornar o arquivo do script executável, nós utilizamos o comando `chmod` com a opção `+x`:

```
:~$ chmod +x <arquivo do script>
```

## 1.6 - Olá, mundo!

Seguindo a boa e velha tradição do *hello, world*, vamos criar o nosso primeiro script em PHP-CLI!

Primeiro, abra o editor de textos da sua preferência. Nós sugerimos o `nano` ou o `vim`, mas pode ser qualquer editor de textos sem formatação (texto plano, *raw text*). Com o editor aberto, digite o código abaixo:

```
#!/usr/bin/env php
<?php

echo "Olá, mundo!\n";
```

Como dissemos, a primeira linha chamará o interpretador de comandos. Isso é um requisito para que o comando `php` seja dispensado na hora de você executar o seu script.

Na segunda linha nós temos outra exigência de qualquer código em PHP, seja para web ou não, que é a tag `<?php`. Ela indica onde o interpretador deverá começar a entender as instruções como código em PHP. Se não houver nada diferente de código em PHP até o final do arquivo, não é preciso fechar a tag com `?>`, mas ela também seria obrigatória no caso de haver tags em HTML ou outros textos quaisquer depois do código em PHP.

Em seguida, aparece a nossa primeira instrução em PHP: a instrução `echo`, que imprime na saída (no terminal ou no navegador web) a expressão que aparecer depois dela como um argumento. No nosso caso, a expressão é a string (cadeia de caracteres) `"Olá,  mundo!\n"`.

Repare que no final da instrução aparece um `;`. Isso é obrigatório ao final de cada instrução em PHP! Nós podemos até escrever as nossas instruções em várias linhas, mas elas só serão interpretadas como tal quando o PHP encontrar o ponto e vírgula ou o fim de um agrupamento de instruções, o que é feito entre chaves (`{ ... }`).

Agora nós só temos que salvar o nosso programa com o nome `ola-mundo.php`. A extensão `.php` não é obrigatória, poderia ser literalmente qualquer coisa ou nada, porque o shell da linha de comandos não reconhece extensões -- para ele, o ponto é um caractere como outro qualquer no nome do arquivo. Mesmo assim, nós vamos utilizar a extensão `.php` neste primeiro exemplo para que nós, humanos, possamos nos organizar e saber que aquele arquivo contém um código em PHP.

Com o arquivo salvo, chegou a hora de torná-lo executável, o que será feito com o comando abaixo no terminal:

```
:~$ chmod +x ola-mundo.php
```

Pronto! Já temos um programa em PHP prontinho para ser executado! Para isso, estando na pasta onde ele foi criado, basta executar:

```
:~$ ./ola-mundo.php
```

Caso esteja em outra pasta, você o executa indicando o caminho completo até ele:

```
:~$ /caminho/até/o/programa/ola-mundo.php
```

Ou ainda, se ele estiver numa pasta que o seu sistema reconhece como um local onde procurar arquivos executáveis (descrito na variável de ambiente `PATH`), bastaria digitar:

```
:~$ ola-mundo.php
```

Se tudo correr bem, você deve ver no seu terminal:

```
:~$ ola-mundo.php
Olá, mundo!
```

# 2 - Interagindo com o shell

Neste módulo, nós veremos as principais formas de exibir saídas e obter dados a partir da linha de comando do terminal, bem como podemos utilizar os comandos e utilitários disponíveis no shell em nossos scripts. 

## 2.1 - Exibindo saídas no terminal

Todos os recursos para exibição de mensagens no terminal são exatamente os mesmos que utilizaríamos para exibir textos em páginas HTML:

* `echo` - exibe uma ou mais strings
* `print` - exibe uma string
* `printf` - exibe strings formatadas
* `print_r` - exibe informações sobre variáveis de forma legível

Todas essas funções (`printf` e `print_r`) e construtores de linguagens (`echo` e `print`) exibirão informações na saída, seja ela o terminal ou um documento HTML gerado dinamicamente no servidor web.

> **Nota:** uma *string* é uma cadeia de caracteres.

### 2.1.1 - Construtores 'echo' e 'print'

Os construtores `echo` e `print` fazem exatamente a mesma coisa: exibem uma string na saída. A diferença é que o `echo` pode concatenar várias strings separadas por vírgulas como argumentos, enquanto o `print` aceita apenas um argumento. Além disso, o construtor `print` sempre retorna o valor `1` e o `echo` nunca retorna nada.

Observe os exemplos:

```
php > // Usando o 'echo'...
php > echo "banana ", "laranja ", "abacate";
banana laranja abacate
```

```
php > // Usando o 'print'...
php > print "banana laranja abacate";
banana laranja abacate
```

**Operador de concatenação**

Tanto o `echo` quanto o `print` podem fazer concatenações (a junção de strings) com o operador de concatenação `.` (ponto):

```
php > print "banana"." "."laranja";
banana laranja
php > echo "banana"." "."laranja";
banana laranja
```

**Quebra de linha**

Nenhum dos dois construtores gera uma quebra de linha após a string resultante, o que pode ser resolvido com a concatenação do caractere de controle correspondente à quebra de linha (`\n` em sistemas *unix like*) ou com a constante interna `PHP_EOL`, que sempre armazena a o caractere de quebra de linha correto para cada plataforma:

```
php > echo "banana\nlaranja";
banana
laranja
php > echo "banana".PHP_EOL."laranja";
banana
laranja
```

**Aspas duplas e simples**

As aspas duplas nos permitem concatenar strings com caracteres de controle e ainda expandem os os valores em variáveis:

```
php > echo "banana\nlaranja\n";
banana
laranja
```

```
php > $str='banana';
php > $str2='laranja';
php > print "Você gosta de $str1 e $str2?\n";
Você gosta de banana e laranja?
```

Já as aspas simples tratarão tudo em seu interior como caracteres literais:

```
php > $str='banana';
php > $str2='laranja';
php > print 'Você gosta de $str1 e $str2?\n';
Você gosta de $str1 e $str2?\n
```

**Múltiplas linhas**

Se não quisermos utilizar caracteres de controle para quebrar linhas, nós podemos escrever as strings entre aspas duplas ou simples em várias linhas:

```
echo '
Qual é
o seu
nome?
';
```

O que imprimiria na saída:

```
Qual é
o seu
nome?

```

> **Nota:** as outras diferenças entre as aspas simples e duplas ainda continuarão vigorando.

### 2.1.2 - Concatenando e formatando strings com a função 'printf()'

Outra forma de concatenar strings é com a função `printf()`. 

> **Nota:** por se tratar de uma função incrivelmente versátil e cheia de opções, nós falaremos dela apenas superficialmente neste momento.

Observe o exemplo:

```
php > $str1='banana';
php > $str2='laranja';
php > printf("Você gosta de %s?", $str1);
Você gosta de banana?
php > printf("Não, eu prefiro %s.", $str2);
Não, eu prefiro laranja.
```

No exemplo, o `%s` é chamado de **especificador de formato**. A função `printf()` trabalha com vários especificadores de formato, e eles servem para "guardar o lugar" dos valores que serão informados nos demais argumentos. O importante no momento, porém, é que cada especificador deve corresponder com o tipo de dado que o valor expressa (strings, números, etc). No nosso caso, como o valor é uma string, o especificador utilizado é o `%s`.

```
php > $str1='banana';
php > $str2='laranja';
php > printf("Eu prefiro %s, mas não odeio %ss.", $str2, $str1);
Não, eu prefiro laranja, mas não odeio bananas.
```

Se fosse um número inteiro, por exemplo, o especificador seria `%d`:

```
php > $nota=10;
php > $aluno="João";
php > printf("%s tirou nota %d", $aluno, $nota);
João tirou nota 10
```

Além disso, os especificadores podem realizar vários tipos de conversões e formatações, por exemplo:

```
php > $valor=3.1415169;
php > printf("%.2f", $valor);
3.14
```

Vale notar que o PHP possui funções internas para quase todas as conversões que o `printf()` é capaz de fazer. A questão, porém, é que essas funções nem sempre fazem a exibição direta do resultado das suas conversões e dependerão de um `echo` ou de um `print` para exibi-los. A propósito, também existe a função `sprintf()`, idêntica à função `printf()`, mas ela retorna uma string sem fazer a sua exibição na saída.

### 2.1.3 - Exibindo o conteúdo de variáveis com a função 'print_r()'

A função `print_r()` exibe o conteúdo de variáveis de forma humanamente legível. Ela não parece muito útil quando tentamos exibir o conteúdo de variáveis escalares (variáveis que apontam para apenas um valor), mas é muito útil quando usada para exibir o conteúdo de veriáveis vetoriais, as chamadas *arrays*:

```
php > $alunos=["Maria", "Pedro", "Antonio"];
php > print_r($alunos);
Array
(
    [0] => Maria
    [1] => Pedro
    [2] => Antonio
)
```

Opcionalmente, nós podemos passar um segundo parâmetro para a função `print_r()` indicando se queremos exibir seu retorno ou apenas guardá-lo em uma variável.

Por padrão, este segundo parâmetro vem definido como `false` (um valor do tipo **booleano**), e é isso que faz com que o retorno seja exibido quando não informamos nenhum outro argumento além do nome da variável. Se quisermos que o retorno da função `print_r()` seja apenas armazenado, nós precisamos incluir o valor booleano `true` como segundo argumento:

```
php > $valores=[1,2,3];
php > print_r($valores);
Array
(
    [0] => 1
    [1] => 2
    [2] => 3
)
php > $saida=print_r($valores, true);
php > echo $saida;
Array
(
    [0] => 1
    [1] => 2
    [2] => 3
)
```

> **Importante!** Como a variável `$valores` é uma array, seu conteúdo não poderia ser exibido com o construtor `echo`:

```
php > echo $valores;
PHP Notice:  Array to string conversion in php shell code on line 1
Array
```

> Já o conteúdo passado para `$saida` é uma string, e foi por isso que o `echo` funcionou no exemplo anterior.

### 2.1.4 - Analisando o valor de expressões com a função 'var_dump()'

Nós podemos obter uma visão mais detalhada sobre expressões com a função `var_dump()`, que exibe de forma estruturada seus valores e tipos.

```
php > $str = 'banana';
php > var_dump($str);
string(6) "banana"
```

Ou ainda...

```
php > $valores=[1,2,3];
php > var_dump($valores);
array(3) {
  [0]=>
  int(1)
  [1]=>
  int(2)
  [2]=>
  int(3)
}
```

## 2.2 - Passando opções na linha de comando com parâmetros posicionais

Uma das formas de passarmos opções para um programa na linha de comando é através dos **parâmetros posicionais**, que são basicamente aqueles argumentos que escrevemos logo após os nomes dos programas, por exemplo:

```
:~$ mv arquivo1.txt arquivo2.txt
```

Na linha de comando do shell, cada uma dessas palavras separadas pelo espaço será tratada como um parâmetro e será numerada conforme a sua posição (daí "posicional"):

```
mv arquivo1.txt arquivo2.txt
 ^     ^            ^
 |     |            |
 0     1            2
```

O parâmetro `0` sempre será o nome do programa ou do comando, e todos os outros parâmetros serão numerados de acordo com sua ordem de aparição.

Para capturar esses parâmetros posicionais de dentro dos nossos scripts nós utilizamos a array `$argv`. Trata-se de um vetor gerado pelo próprio PHP-CLI quando o script é executado na linha de comando, e ele irá conter o nome do script (no índice `0`) e todos os parâmetros passados na linha de comando.

Por exemplo, no script `teste-pars.php`:

```
#!/usr/bin/env php
<?php

print_r($argv);
```

Tudo que ele fará é exibir o conteúdo da array `$argv`. Então, se nós o executarmos na linha de comando sem argumentos:

```
:~$ ./teste-pars.php 
Array
(
    [0] => ./teste-pars.php
)
```

Como podemos ver, o elemento de índice `0` de `$argv` é o nome do próprio script tal como ele foi chamado na linha de comando. Vamos incluir alguns parâmetros e ver o que acontece:

```
:~$ ./teste-pars.php banana laranja abacate
Array
(
    [0] => ./teste-pars.php
    [1] => banana
    [2] => laranja
    [3] => abacate
)

```

Agora, os valores `banana`, `laranja` e `abacate` foram capturados pelo nosso script na array `$argv` e podem ser usados no nosso código. Outro exemplo, o script `hello.php`:

```
#!/usr/bin/env php
<?php

echo "Olá, $argv[1]!\n\n";
```

Sabendo como funcionam os parâmetros posicionais, nós queremos utilizar o elemento de índice `1` em `$argv` na string que será exibida no terminal:

```
$ ./hello.php Blau
Olá, Blau
```

### Contando o número de parâmetros

Às vezes, é preciso saber quantos parâmetros foram passados na linha de comando, ou mesmo se foi passado o número correto de parâmetros. Para isso, nós podemos contar com a variável `$argc`. Modificando o script `teste-pars.php`:

```
#!/usr/bin/env php

<?php

echo "O número de parâmetros passados é: $argc\n\n";

print_r($argv);

?>
```

Vamos executá-lo novamente:

```
:~$ ./teste-pars.php banana laranja abacate

O número de parâmetros passados é: 4

Array
(
    [0] => ./teste-pars.php
    [1] => banana
    [2] => laranja
    [3] => abacate
)

```

Como podemos observar, o parâmetro `0` (o nome do script) também foi incluído na contagem, e isso deve ser levado em consideração nos nossos códigos.


## 2.3 - Lendo a entrada padrão

Outra forma de obtermos informações do usuário na linha de comando é solicitando que ele digite algo que o nosso script irá processar posteriormente. Isso pode ser feito de várias formas no PHP-CLI, mas no momento vamos nos concentrar na forma mais simples, que é com a função `readline()`.

A função `readline()` irá pausar a execução do script até que o usuário digite alguma coisa e tecle `[Enter]`. Tudo que o usuário digitar (exceto o `[Enter]`) será retornado pela função e poderá ser armazenado numa variável. Por exemplo:

```
php > $var=readline("Digite algo: ");
Digite algo: teste
php > echo $var;
teste
```

O parâmetro opcional de `readline()` é uma string que servirá de orientação sobre o que o usuário deverá informar, ou "*prompt*". No nosso caso, foi `"Digite algo: "`. 

Então, vamos modificar o nosso script `hello.php` para que ele pergunte o nome do usuário:

```
#!/usr/bin/env php

<?php

$nome=readline("Digite o seu nome: ");

echo "Olá, $nome!\n\n";

?>
```

Executando...

```
$ ./hello.php 

Digite o seu nome: Blau Araujo
Olá, Blau Araujo!

```

## 2.4 - Executando comandos externos

Uma das maiores forças do PHP-CLI está na possibilidade de usar recursos do próprio shell nos nossos códigos e interagir com ele.

Por exemplo, se eu estiver no terminal quiser saber o meu nome de usuário, eu executo o comando `whoami`:

```
:~$ whoami
blau
```

O PHP-CLI nos oferece diversas formas de acessar os comandos do shell nos nossos scripts. Neste momento, vamos conhecer apenas as suas principais: as funções `exec()` e `system()`.

As duas fazem a mesma coisa, só que apenas a função `system()` exibe diretamente a saída do comando externo. Por exemplo:

```
php > system("whoami");
blau
php > maquina=exec("hostname");
php > echo $maquina;
enterprise
```

> **Nota:** repare que a linha de comando executada deve estar entre parêntesis.

Aproveitando que aprendemos mais esse recurso do PHP-CLI, vamos incrementar o nosso script `hello.php`:

```
#!/usr/bin/env php

<?php

$nome=readline("Digite o seu nome: ");

echo "Olá, $nome!\n\n";

echo "Informações do Sistema\n\n";

echo "Usuário     : ".exec("whoami")."\n";
echo "Hostname    : ".exec("hostname")."\n";
echo "Tempo ligado: ".exec("uptime -p")."\n";
echo "Kernel      : ".exec("uname -rms")."\n\n";

?>
```

Executando...

```
$ ./hello.php 

Digite o seu nome: Blau Araujo
Olá, Blau Araujo!

Informações do Sistema

Usuário     : blau
Hostname    : enterprise
Tempo ligado: up 1 day, 6 hours, 58 minutes
Kernel      : Linux 4.19.0-9-amd64 x86_64

```

# 3 - Variáveis e tipos de dados

Uma variável é uma região na memória onde uma certa informação é armazenada temporariamente. No PHP, esses locais são identificados por nomes precedidos pelo cifrão (`$`) e a forma de atribuição mais básica é feita com o operador de atribuição `=`.

## 3.1 - Nomeando variáveis

Os nomes de variáveis devem começar com letras maiúsculas, minúsculas ou o caractere sublinhado seguidos por qualquer quantidade de letras, números e sublinhados. O PHP aceita nomes de variáveis contendo caracteres da tabela ASCII estendida (bytes de 128 a 255), mas utilizar acentos, cedilha e outros símbolos gráficos, embora funcionem, geralmente não é considerado uma boa prática.


**Exemplo 1:**

```
php > $var = 'banana';
php > $Var = 'laranja';
```

Como os nomes de variáveis são sensíveis à caixa, `$var` e `$Var` identificam duas variáveis diferentes:

```
php > echo "$var $Var";
banana laranja
```

**Exemplo 2:**

```
php > $_nome = 'Maria';   // Nome válido
php > $ação = 'copiar';   // Nome válido
php > $_2var = 'teste';   // Válido, começa com '_'
```

**São proibidos:**

* Números no início do nome após o cifrão
* O nome da variável especial `$this`

**Exemplo 3:**

```
php > $2url = 'teste.org' // Inválido, começa com número
```

> **Nota:** Para eliminar variáveis, nós podemos utilizar a função `unset()`:

```
php > $a = 'banana';
php > echo $a;
banana
php > unset($a);
php > echo $a;
PHP Notice:  Undefined variable: a in php shell code on line 1
```

## 3.2 - Expressões

As expressões são os elementos mais importantes do PHP, e quase tudo que escrevemos no nosso código são expressões. De forma bem objetiva, o manual do PHP define expressões como: "qualquer coisa que tenha um valor", ou seja, através das expressões que escrevemos, nós representamos valores.

As formas mais básicas de expressões são as variáveis e as constantes. Quando escrevemos `$a = 10`, nós estamos atribuindo o valor `10` ao nome `$a`, onde `10` é a expressão que representa o valor `10`. Após a atribuição, `$a` também terá o valor `10` e, se escrevermos `$b = $a`, nesta atribuição, `$a` será uma expressão que representa o valor `10`.

Indo um pouco mais fundo, a própria atribuição `$a = 10` é uma expressão e também representa um valor (10, neste exemplo). Isso significa que podemos fazer algo como:

```
php > $c = $a = 10;
php > echo $c;
10
```

Nós falaremos mais sobre isso adiante, mas é óbvio que nem todos os valores serão números inteiros. O PHP suporta 4 tipos de valores escalares, ou seja, valores que não podem ser decompostos em outros valores: 

* Números inteiros (integer)
* Números de ponto flutuante (float)
* Strings (cadeias de caracteres)
* Valores booleanos

Além disso, há dois tipos de valores não-escalares (ou compostos):

* Vetores (arrays)
* Objetos 

Todos esses tipos de valores podem ser atribuídos a uma variável ou retornar de uma função.


## 3.3 - Atribuição por valor e por referência

No PHP, as variáveis podem ser atribuídas **por valor** ou **por referência**.

**Atribuição por valor**

Quando uma expressão é atribuída a uma variável, todo o valor original da expressão é copiado para a variável. Neste caso, nenhuma atribuição subsequente desta variável a outras variáveis, que porventura venham a ser alteradas posteriormente, afetarão o valor da variável original:

```
php > $a = 10;
php > $b = $a;
php > echo "$a, $b";
10, 10
php > $b = $a + 1;
php > echo "$a, $b";
10, 11
```

> **Esta é a forma padrão de atribuição de variáveis no PHP.**

**Atribuição por referência**

É quando uma variável recebe a referência a outra variável em vez do seu valor, como se estivéssemos criando um apelido (alias) ou apontando para a variável original. Portanto, mudanças nesta variável também afetarão a variável original:

```
php > $a = 10;
php > $b = &$a;
php > $b++;
php > echo "$a, $b";
11, 11
```

## 3.4 - Variáveis não inicializadas

Não é obrigatório inicializar variáveis no PHP, mas é uma ótima prática. Variáveis não inicializadas assumirão o valor padrão de seu tipo dependendo do contexto em que são usadas:

* **Booleanos**: false
* **Integers e floats**: 0
* **Strings**: string vazia
* **Arrays**: array vazia

## 3.5 - Escopo das variáveis

Nós podemos entender "escopo" como os limites da disponibilidade de uma variável no nosso código. De modo geral, todas as variáveis no PHP têm escopo **global**, menos nas funções definidas pelo usuário, onde todas as variáveis são limitadas ao escopo **local** da função.

Neste exemplo, as variáveis `$a` e `$b` possuem escopo **global** e estão disponíveis em qualquer ponto da sessão, mas não são visíveis no interior da função soma, o que gera os erros de variável indefinida:

```
php > $a=1; $b=2;
php > function soma() { return $a + $b; }
php > echo $a + $b;
3
php > echo soma();
PHP Notice:  Undefined variable: a in php shell code on line 1
PHP Notice:  Undefined variable: b in php shell code on line 1
0
```

Para utilizarmos variáveis globais dentro de funções, elas precisam ser declaradas com a palavra chave `global`:

```
php > $a=1; $b=2;
php > function soma() { global $a, $b; return $a + $b; }
php > echo soma();
3
```

Outra forma de acessar as variáveis do escopo global é através da variável especial do PHP `$GLOBALS`, uma array associativa com os nomes das variáveis globais nos índices e seus respectivos conteúdos nos valores:

```
php > $a=1; $b=2;
php > function menos() { return $GLOBALS['b'] - $GLOBALS['a']; }
php > echo menos();
1
```

> **Nota:** A variável `$GLOBALS` existe em qualquer escopo e é o que se chama de **superglobal** no PHP -- variáveis internas do PHP e constantes que estão sempre disponíveis em todos os escopos.

Ainda nas funções, nós podemos declarar outro tipo variável local: as variáveis estáticas. Uma variável estática só existe no escopo local da função, mas não perde seu valor quando o programa sai desse escopo. Por exemplo:

```
php > function contador() { $a = 0; echo $a; $a++; }
php > contador();
0
php > contador();
0
php > contador();
0
...
```

Toda vez que a função `contador()` é chamada, o valor de `$a` torna-se `0` novamente e, portanto, o incremento (`$a++`) não acontece. Para que o valor atual não se perca, é possível declarar a variável `$a` como estática com o modificador `static`:

```
php > function contador() { static $a = 0; echo $a; $a++; }
php > contador();
0
php > contador();
1
php > contador();
2
php > contador();
3
...
```

> **Nota:** Os modificadores `global` e `static` implementam uma **referência** às variáveis fora do escopo local da função, ou seja, são como *apelidos* das variáveis originais.

## 3.6 - Variáveis variáveis

As variáveis também poder ser definidas e usadas dinamicamente no PHP, ou seja, seus valores podem ser usados como nomes de outras variáveis. Para isso, a atribuição deve ser feita com dois cifrões (`$$`) antes do nome da variável cujo valor queremos usar. Por exemplo:

```
php > $str= 'Olá';
php > $$str = 'mundo';
php > echo "$str, ${$str}!";
Olá, mundo!
```

No caso, `${$str}` é o mesmo que:

```
echo $Olá;
mundo
```

No caso das arrays, é preciso especificar o que queremos. Veja o exemplo:

```
php > $frutas = ['banana', 'laranja', 'abacate'];
php > $$frutas[1] = 'pera';
PHP Notice:  Array to string conversion in php shell code on line 1
```

O interpretador não teria como saber se queremos usar o valor em `$frutas[1]` como o nome da nova variável, ou se estamos tentando usar toda a array `$frutas` como nome (daí o erro de conversão de array para string) para, dessa nova variável, obter o índice `1`. 

Para resolver essa ambiguidade, nós podemos escrever:

```
php > ${$frutas[1]} = 'pera';
php > echo ${$frutas[1]};
pera
```

O que equivale a...

```
php > echo $laranja;
pera
```

## 3.7 - Tipos de dados

Quanto aos tipos de dados que uma variável pode conter, nós já vimos que o PHP os detecta automaticamente a partir do contexto em que as variáveis aparecem. Também vimos que seus tipos podem ser do tipo **booleano**, **inteiro**, **flutuante** e **string**. No caso das arrays, os tipos dizem respeito aos seus elementos, ou seja, cada um de seus elementos pode ser de qualquer um desses quatro tipos.

### Tipo booleano

Um dado do tipo booleano expressa a verdade de uma expressão, que pode ser verdadeira (`true`) ou falsa (`false`). Para expressar um valor booleano de forma literal, nós usamos as constantes `TRUE` e `FALSE`, que podem ser escritas indiferentemente em maiúsculas ou minúsculas.

```
$a = true;
$b = True;
$c = TRUE;
```

Embora geralmente desnecessário, também podemos converter valores em booleanos usando os modificadores `(bool)` ou `(boolean)`. Neste caso, todos os valores abaixo seriam considerados `false`:

* O próprio booleano `FALSE`
* Os inteiros `0` e `-0`
* Os flutuantes `0.0` e `-0.0`
* Strings vazias e a string `0`
* Uma array sem elementos
* O tipo especial `NULL` e variáveis indefinidas
* Objetos `SimpleXML` criados a partir de tags vazias

Todos os demais valores são considerados `TRUE`.

> **Nota:** Os modificadores de conversão de tipos, sejam eles quais forem, são utilizados imediatamente antes do acesso ao nome das variáveis:

```
php > $a = 2;
php > echo (bool)$a;
1
```

### Tipo inteiro

Inteiros são os números naturais e seus simétricos negativos. Eles podem ser especificados nas bases 10 (decimal), 16 (hexadecimal), 8 (octal) ou 2 (binário):

* **Base 16**: precedido por `0x`
* **Base 8**: precedido por `0`
* **Base 2**: precedido por `0b`

> **Nota:** A partir do PHP 7.4.0, inteiros literais podem conter caracteres de sublinhado (`_`) entre os dígitos para facilitar a legibilidade.

O tamanho da integer depende da plataforma, mas o limite usual é de cerca de 2.14 bilhões positivos ou negativos o que corresponde a 32 bits com sinal (o PHP não suporta inteiros sem sinal).

Nós podemos ver o tamanho máximo dos inteiros na nossa plataforma com a constante `PHP_INT_MAX`:

```
php > echo PHP_INT_MAX;
9223372036854775807
```

Também podemos ver quantos bytes são alocados para uma integer com a constante `PHP_INT_SIZE`:

```
php > echo PHP_INT_SIZE;
8
```

Para converter explicitamente um valor em inteiro, nós podemos usar os modificadores `(int)` ou `(integer)`. Geralmente, isso é desnecessário, já que os valores serão automaticamente convertidos se um operador, uma função ou uma estrutura de controle requererem um inteiro como argumento.

Na conversão...

* `FALSE` é convertido para `0` e `TRUE` para `1`.
* Valores flutuantes retornam apenas a parte inteira.
* Valores flutuantes além dos limites dos inteiros o resultado será indefinido.
* Se a string não contiver `.`, `e`, `E`, e o valor estiver nos limites de `PHP_INT_MAX`, ela será convertida para inteiro.
* O tipo especial `NULL` sempre será convertido para `0`.

### Números de ponto flutuante

Números de ponto flutuante (também chamados de "floats", "doubles" ou "números reais") são aqueles que possuem casas decimais e podem ser expressos no PHP como:

```
$f = 1.234;
$f = 1.2e3;
$f = 5E-10;
$f = 1_234.567; // (PHP >= 7.4.0)
```

O valor máximo de um número flutuante pode ser visto com a constante `PHP_FLOAT_MAX` e é dependente da plataforma:

```
php > echo PHP_FLOAT_MAX;
1.7976931348623E+308
```

> **Nota:** Números de ponto flutuante possuem precisão limitada e não podemos confiar em seus resultados até o último dígito.

Eventualmente, algumas operações numéricas podem resultar em um valor representado pela constante `NAN`, que expressa um valor indefinido ou incapaz de ser representado em cálculos envolvendo pontos flutuantes, o que pode ser verificado com a função `is_nan()`.

### Strings

Uma string é uma cadeia de caracteres, onde cada caractere tem o tamanho de um byte. Isso significa que o PHP suporta apenas um conjunto de 256 caracteres e, portanto, não oferece suporte a caracteres Unicode nativamente. 

Todos os valores sempre serão tratados como strings no contexto de uma expressão que precise de uma string, como em um `echo` ou um `print`, por exemplo, ou quando o valor de uma variável for comparado com uma string. Mesmo assim, podemos converter valores em strings com o modificador `(string)` ou a função `strval()`.

Na conversão...

* `TRUE` é convertido para `"1"` e `FALSE` é convertido para `""`.
* Inteiros e flutuantes são convertidos para a representação textual de seus valores.
* Arrays são sempre convertidas para a string `"Array"`.
* `NULL` é sempre convertida para uma string vazia.

Uma string literal pode ser expressa de 4 formas diferentes:

* Entre aspas simples
* Entre aspas duplas
* Por um *heredoc* (equivale a aspas duplas)
* Por um *nowdoc* (equivale a aspas simples)

**Aspas simples**

A forma mais simples de representar uma string é com as aspas simples. Para escapar (tornar literais) as aspas simples em uma string entre aspas simples, nós utilizamos o caractere `\` (barra invertida). Para expressar uma barra invertida literal, nós escrevemos `\\`. Qualquer outra ocorrência da barra invertida será tratada como uma representação literal, ou seja, caracteres de controle (como `\n` ou `\r`), por exemplo, serão tratados literalmente:

```
php > echo 'banana\n\'laranja\'\nabacate\\';
banana\n'laranja'\nabacate\
```

**Aspas duplas**

É possível escapar aspas duplas e a barra invertida, mas os caracteres de controle serão interpretados:

```
php > echo "banana\n\"laranja\"\nabacate\\";
banana
"laranja"
abacate\
```

Além disso, as variáveis que aparecerem numa string entre aspas duplas serão expandidas, a menos que o `$` de seus nomes sejam escapados:

```
php > $a = 'laranja';
php > echo "banana $a";
banana laranja
php > echo "banana \$a";
banana $a
```

**Heredoc**

O *heredoc* tem o mesmo efeito das aspas duplas envolvendo uma string em várias linhas:


```
php > $frutas = "
php " banana
php " laranja
php " abacate
php " ";
php > echo $frutas;

banana
laranja
abacate
```

Com heredoc:

```
php > $bichos = <<<FIM
<<< > leão
<<< > zebra
<<< > elefante
<<< > FIM;
php > echo $bichos;
leão
zebra
elefante
```

**Nowdoc**

Um *nowdoc* tem o mesmo efeito das aspas simples envolvendo uma string em várias linhas. A diferença para as heredocs é que o identificador aparece entre aspas simples:


```
php > $carros = <<<'FIM'
<<< > kia
<<< > peugeot
<<< > ferrari
<<< > FIM;
php > echo $carros;
kia
peugeot
ferrari
```

## 3.8 - Arrays

Sem nos aprofundarmos muitos em detalhes sobre tipos de dados que fogem dos nossos propósitos, nós podemos definir arrays como uma estrutura de dados que associa valores a índices. As arrays podem ser criadas usando o construtor de linguagem `array()`, onde são especificados pares de índices e valores:

```
$arr = array(
  'nome'  => 'João',
  'idade' => 23,
  'nota'  => 7.5
);
```

> **Nota:** A vírgula depois da especificação **do último elemento** não é obrigatória e deve ser preferencialmente dispensada quando a instrução é dada em uma linha.

A partir do PHP 5.4, também podemos utilizar a sintaxe abreviada `[ ]`:

```
$arr = [
  'nome'  => 'João',
  'idade' => 23,
  'nota'  => 7.5
];
```

De qualquer forma, a chave pode ser uma string ou um inteiro, e o valor pode conter valores de qualquer tipo. Além disso, as seguintes transformações também vão ocorrer nos índices:

* Strings expressando valores inteiros decimais válidos e sem sinal serão convertidas para inteiro.
* Números flutuantes serão truncados e convertidos para inteiros.
* Valores booleanos serão convertidos para inteiros.
* `NULL` será convertida para uma string vazia.

Se vários elementos forem declarados com o mesmo índice, apenas o último será usado e os demais serão sobrescritos:

```
php > $arr = [
php > 1 => 'a',
php > '1' => 'b',
php > 1.5 => 'c',
php > true => 'd'
php > ];
php > print_r($arr);
Array
(
    [1] => d
)
```

A especificação de um índice é opcional no PHP, e ainda é possível definir o índice de alguns elementos e deixar os demais indefinidos. Quando os índices não são especificados, o PHP irá utilizar um incremento do último maior valor inteiro usado anteriormente.

```
php > $arr = ['banana', 'laranja', 'pera'];
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => pera
)
```

> **Nota:** Arrays e objetos não podem ser usados como índices!

O PHP também permite o uso de arrays como valores de elementos de uma array, e isso é o que nos permite trabalhar com arrays multidimensionais:

```
php > $alunos = [
php > ['nome' => 'João', 'idade' => 23, 'nota' => 7.5],
php > ['nome' => 'Maria', 'idade' => 25, 'nota' => 8.0],
php > ['nome' => 'Pedro', 'idade' => 22, 'nota' => 7.0]
php > ];
php > print_r($alunos);
Array
(
    [0] => Array
        (
            [nome] => João
            [idade] => 23
            [nota] => 7.5
        )

    [1] => Array
        (
            [nome] => Maria
            [idade] => 25
            [nota] => 8
        )

    [2] => Array
        (
            [nome] => Pedro
            [idade] => 22
            [nota] => 7
        )

)
```

Os valores de uma array podem ser modificados pela especificação do índice do elemento que queremos alterar:

```
php > $arr = ['banana', 'laranja', 'pera'];
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => pera
)
php > $arr[2] = 'abacate';
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => abacate
)
```

Também podemos incluir novos elementos em uma array sem especificar um índice:

```
php > $arr[3] = 'pera'; // incluindo elemento com índice
php > $arr[] = 'limão'; // incluindo elemento sem índice
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => abacate
    [3] => pera
    [4] => limão
)
```

Para remover um elemento da array, nós usamos a função `unset()`:

```
php > unset($arr[2]);
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [3] => pera
    [4] => limão
)
```

## 3.9 - Constantes

Constantes são apenas identificadores de valores e, como o nome sugere, esses valores não podem mudar durante a execução do script. Por convenção, os identificadores das constantes são sempre expressos em maiúsculas.

Para criar uma constante, nós utilizamos a função `define()`:

```
define('MINHA_CONSTANTE', 'qualquer valor');
```

Independente do escopo, as constantes são sempre globais, ou seja, podem ser acessadas de qualquer lugar no script:

```
php > define('MINHA_CONSTANTE', 10);
php > function soma() { return MINHA_CONSTANTE + 5; }
php > echo soma();
15
```


# 4 - Trabalhando com arquivos e pastas

O PHP oferece diversas funções para navegar, listar, criar, ler e alterar arquivos e pastas. Nesta aula, nós tentaremos esboçar uma espécie de guia rápido de consulta, agrupando as principais funções de acordo com as tarefas que quisermos executar.

## 4.1 - Listar diretórios

Existem essencialmente duas formas de lidar com a listagem de diretórios e seus conteúdos: através da criação de um manipulador (*handler*) de diretórios, que é a forma mais tradicional, ou através da função `scandir()`, que retorna a listagem do diretório sob a forma de uma array. 

Obviamente, a função `scandir()` torna o nosso trabalho muito mais fácil, mas a operação com manipuladores pode nos dar mais controle sobre o que realmente queremos de uma listagem de diretórios.

### 4.1.1 - Função 'scandir()'

```
scandir('DIR' [, ORDEM])
```

Retorna uma array com a listagem de pastas e arquivos encontrados em DIR.

**DIR**

É o caminho da pasta que queremos listar.

**ORDEM**

Parâmetro opcional que indica a ordem da listagem:

* **SCANDIR_SORT_ASCENDING** - Alfanumérica ascendente (padrão)
* **SCANDIR_SORT_DESCENDING** - Alfanumérica descendente
* **SCANDIR_SORT_NONE** - Sem ordenação

### 4.1.2 - Listando pastas e arquivos com um manipulador

O processo tradicional de listagem de diretórios envolve três etapas: abrir o diretório associando um manipulador a ele, ler recursivamente o conteúdo do diretório e, ao final, fechar o manipulador criado. De forma geral, este seria o procedimento:

```
$h = opendir(CAMINHO);

while (false !== ($e = readdir($h))) {
    echo $e.PHP_EOL;
}

closedir($h);
```

Repare que o processo envolve três funções:

* `opendir()` - Abre CAMINHO para leitura e retorna um manipulador.
* `readdir()` - Lê uma entrada no diretório listado.
* `closedir()` - Fecha o manipulador. 

A cada chamada da função `readdir()` o ponteiro da listagem passa para a entrada seguinte no diretório, por isso é comum que ela seja utilizada dentro de um laço de repetição, o que permite uma leitura sequencial de cada entrada. 

Também é importante notar que `opendir()` e `readdir()` retornam `FALSE` caso haja algum problema, e isso pode ser usado para determinar, por exemplo, se CAMINHO não existe ou está inacessível, ou se já não há mais entradas no diretório a serem listadas. Portanto, a forma mais comum de listar o conteúdo de uma pasta seria:

```
// Se 'opendir()' retornar FALSE, nada é executado...
if ($h = opendir(CAMINHO)) {

    // Se 'readdir()' retornar o tipo booleano FALSE, o loop para...
    while (false !== ($e = readdir($h))) {
        echo $e.PHP_EOL;
    }

    closedir($h);
}
```

> **Nota:** O operador de comparação `!==` avalia como verdadeiro se ambos os termos forem diferentes em valor e tipo.

## 4.2 - Buscar arquivos e pastas segundo um padrão

O processo de listar arquivos e pastas filtrando o resultado através de padrões é chamado de **globbing**, e o PHP tem uma função para lidar justamente com isso, a função `glob()`, que retorna uma array com os resultados encontrados.

```
glob('PADRÃO' [, FLAGS])
```

Retorna uma array com os resultados que correspondam a PADRÃO.

**PADRÃO**

O padrão de filtragem é uma string que utiliza basicamente os mesmos símbolos (metacaracteres) utilizados na linha de comando do terminal.

**METACARACTERES**

* `*` - Zero ou mais caracteres quaisquer
* `?` - Exatamente um caractere
* `[LISTA]` - Um caractere existente na lista
* `[!LISTA]` - Qualquer caractere fora da lista

Além disso, podemos escapar os caracteres que devem ser considerados literais na busca com a barra invertida (`\`).

**FLAGS**

Também podemos modificar o comportamento padrão da função incluindo FLAGS:

* **GLOB_MARK** - Inclui uma barra no resultado se for diretório.
* **GLOB_NOSORT** - Não ordena resultados.
* **GLOB_NOCHECK** - Retorna PADRÃO se não hover correspondência.
* **GLOB_NOESCAPE** - A barra invertida passa a ser literal.
* **GLOB_BRACE** - Habilita a "lista  ou" entre chaves.
* **GLOB_ONLYDIR** - Lista apenas diretórios.
* **GLOB_ERR** - Para a busca se houver erros.

## 4.3 - Comparar nomes de arquivos com um padrão

Quando não temos um nome exato para fazer uma comparação, é muito útil termos a alternativa de fazer a comparação através de um padrão, e é para isso que serve a função `fnmatch()`.

```
fnmatch('PADRÃO', 'NOME'[, FLAGS])
```

Retorna `TRUE` ou `FALSE` como resultado da comparação de PADRÃO com NOME.


**PADRÃO**

Uma string de comparação usando os mesmos metacaracteres da função `glob()`.

**NOME**

Nome do arquivo que está sendo comparado.

**FLAGS**

* **FNM_NOESCAPE** - A barra invertida passa a ser literal.
* **FNM_PATHNAME** - A barra normal em NOME só bate com a barra normal em PADRÃO.
* **FNM_PERIOD** - Ponto no início de NOME tem que bater com um ponto em PADRÃO.
* **FNM_CASEFOLD** - Ignora caixa alta ou baixa.

## 4.4 - Testar atributos de arquivos

O PHP oferece várias funções para testar atributos de arquivos, todas retornando `TRUE` caso o teste tenha uma correspondência verdadeira com o atributo testado.

* `is_dir('ARQUIVO')` - TRUE se for diretório
* `is_file('ARQUIVO')` - TRUE se for arquivo comum
* `is_link('ARQUIVO')` - TRUE se for um link simbólico
* `is_executable('ARQUIVO')` - TRUE se for executável
* `is_readable('ARQUIVO')` - TRUE se for possível ler o arquivo
* `is_writable('ARQUIVO')` - TRUE se for possível escrever no arquivo
* `file_exists('ARQUIVO')` - TRUE se ARQUIVO existir

> **Nota:** Lembre-se de que, no sistema de arquivos, tudo é arquivo, inclusive as pastas.

## 4.5 - Obtendo informações de caminhos

No PHP, é possível obter partes de um caminho que correspondam ao nome de um arquivo ou uma pasta, o diretório onde ele está, a extensão do arquivo ou o nome sem a extensão.

### 4.5.1 - Função 'getcwd()'

```
getcwd()
```

Retorna o caminho completo do diretório de trabalho corrente.

### 4.5.2 - Função 'dirname()'

```
dirname('CAMINHO'[, NÍVEIS])
```

Dado um CAMINHO, retorna tudo até antes da sua última parte, seja ela uma pasta ou um nome de arquivo:

```
php > echo dirname('/home/user/teste/');
/home/user

php > echo dirname('/home/user/teste/arquivo.php');
/home/user/teste
```

Opcionalmente, podemos definir até quantos níveis antes da parte final nós queremos que a função retorne (o padrão é 1).

```
php > echo dirname('/home/user/teste/', 2);
/home

php > echo dirname('/home/user/teste/arquivo.php', 2);
/home/user
```

### 4.5.3 - Função 'basename()'

```
basename('CAMINHO'[, 'SUFIXO'])
```

Retorna a última parte de CAMINHO.

```
php > echo basename('/home/user/teste/');
teste

php > echo basename('/home/user/teste/arquivo.php');
arquivo.php
```

Opcionalmente, podemos eliminar um SUFIXO da string retornada:

```
php > echo basename('/home/user/teste/arquivo.php', '.php');
arquivo
```

### 4.5.4 - Função 'pathinfo()'

```
pathinfo('CAMINHO'[, OPÇÃO])
```

Retorna uma array com  informações de CAMINHO.

```
php > print_r (pathinfo('/home/user/teste/arquivo.php'));
Array
(
    [dirname] => /home/user/teste
    [basename] => arquivo.php
    [extension] => php
    [filename] => arquivo
)
```

**OPÇÕES**

Opcionalmente, podemos especificar as informações que serão retornadas.

* **PATHINFO_DIRNAME** - Equivale à função `dirname()`.
* **PATHINFO_BASENAME** - Equivale à função `basename()`.
* **PATHINFO_FILENAME** - Retorna nome do arquivo sem a extensão.
* **PATHINFO_EXTENSION** - Retorna apenas a extensão.

## 4.6 - Operações comuns com arquivos

Também podemos realizar as mesmas operações mais simples com pastas e arquivos que executamos na linha de comando de um terminal.

### 4.6.1 - Função 'chdir()'

```
chdir('DIR')
```

Altera o diretório de trabalho para DIR.

### 4.6.2 - Função 'copy()'

```
copy('ORIGEM', 'DESTINO')
```

Copia arquivos e pastas em ORIGEM para DESTINO.

### 4.6.3 - Função 'rename()'

```
rename('ORIGINAL', 'NOVO')
```

Move arquivos e pastas descritos em ORIGINAL para o local ou o nome NOVO.

### 4.6.4 - Função 'unlink()'

```
unlink('ARQUIVO')
```

Remove arquivos e pastas.

### 4.6.5 - Função 'chmod()'

```
chmod('ARQUIVO', PERMISSÂO)
```

Atribui a ARQUIVO uma PERMISSÃO em formato octal.


## 4.7 - Leitura e escrita de arquivos

Assim como o trabalho com diretórios, o PHP oferece duas abordagens para lidar com a escrita e leitura de arquivos: a tradicional, com a criação de um manipulador, ou com o uso de várias outras funções mais objetivas -- porém, menos flexíveis.

### 4.7.1 - Manipulação básica de arquivos

O processo básico de manipulação básica de arquivos envolve, como antes, três estágios: a abertura do arquivo para uma finalidade (leitura, escrita, etc...) com a criação de um manipulador, a leitura ou a escrita no arquivo, e o fechamento do arquivo e seu manipulador.

A função utilizada para abrir o arquivo e retornar um manipulador é a `fopen()`, e todo arquivo aberto deve ser fechado ao final das operações com a função `fclose()`.

```
// Abre arquivo no modo MODO...
$h = fopen('ARQUIVO', 'MODO');

... INSTRUÇÕES ...

// Fecha o manipulador do arquivo...
fclose($h);
```

Os modos são definidos por uma string que indica:

* **'r'** - Abre somente para leitura e coloca o ponteiro do arquivo no começo do arquivo.

* **'r+'** - Abre para leitura e escrita e coloca o ponteiro do arquivo no começo do arquivo.

* **'w'** - Abre somente para escrita, coloca o ponteiro do arquivo no começo do arquivo e reduz o comprimento do arquivo para zero -- se o arquivo não existir, tenta criá-lo.

* **'w+'** - Abre para leitura e escrita, coloca o ponteiro do arquivo no começo do arquivo e reduz o comprimento do arquivo para zero -- se o arquivo não existir, tenta criá-lo.

* **'a'** - Abre somente para escrita e coloca o ponteiro do arquivo no final do arquivo -- se o arquivo não existir, tenta criá-lo.

* **'a+'** - Abre para leitura e escrita e coloca o ponteiro do arquivo no final do arquivo -- se o arquivo não existir, tenta criá-lo.

* **'x'** - Cria e abre o arquivo somente para escrita e coloca o ponteiro no começo do arquivo. Se o arquivo já existir, a chamada a fopen() falhará, retornando FALSE e gerando um erro E_WARNING. Se o arquivo não existir, tenta criá-lo.

* **'x+'** - Cria e abre o arquivo para leitura e escrita e coloca o ponteiro no começo do arquivo. Se o arquivo já existir, a chamada a fopen() falhará, retornando FALSE e gerando um erro E_WARNING. Se o arquivo não existir, tenta criá-lo. 

#### Funções para leitura

```
fread(MANIPULADOR, BYTES)
```

Lê uma quantidade de BYTES a partir do ponteiro indicado pelo MANIPULADOR ou até chegar ao fim do arquivo ou encontrar pacotes de dados incompletos ou truncados.

```
fgets(MANIPULADOR[, BYTES])
```

Lê uma linha a partir do ponteiro indicado pelo MANIPULADOR ou até encontrar: uma quebra de linha, a quantidade de BYTES menos 1, ou o fim do arquivo.

```
fgetc(MANIPULADOR)
```

Lê apenas um caractere a partir do ponteiro indicado pelo MANIPULADOR.

#### Funções para escrita

```
fwrite(MANIPULADOR, 'STRING'[, BYTES ])
```

Escreve o conteúdo de STRING na posição do ponteiro indicado pelo MANIPULADOR. Opcionalmente, a escrita pode ser interrompida após um dado número de BYTES.

### 4.7.2 - Lendo e escrevendo em arquivos do jeito fácil

Especialmente quando queremos todo o contéudo de um arquivo, ou escrever conteúdos todos de uma vez, é muito mais prático utilizar as funções abaixo.

### Função 'touch()'

```
touch('ARQUIVO')
```

Altera data e hora de acesso a ARQUIVO ou o cria se ele não existir.

### Função 'file()'

```
file('ARQUIVO'[, FLAGS])
```

Retorna uma array com as linhas de ARQUIVO. Opcionalmente, podemos alterar o comportamento padrão da função com algumas FLAGS, como:

* **FILE_IGNORE_NEW_LINES** - Ignora quebras de linha.
* **FILE_SKIP_EMPTY_LINES** - Pula linhas vazias.

### Função 'file_get_contents()'

```
flie_get_contents('ARQUIVO')
```

Retorna todo o conteúdo de ARQUIVO como uma string.

### Função 'file_put_contents()'

```
file_put_contents('ARQUIVO', 'DADOS'[, FLAGS])
```

Escrve todo o conteúdo de DADOS em ARQUIVO. Se ARQUIVO não existir, ele é criado e DADOS são escritos nele.

**DADOS**

O conteúdo a ser escrito no arquivo. Pode ser uma string, uma array ou um stream de dados.

**FLAGS**

* **FILE_APPEND** - Se ARQUIVO existir, inclui DADOS no final do conteúdo.
* **LOCK_EX** - Tranca o arquivo durante a escrita.


## 4.8 - Streams de entrada e saída

Como o nosso foco é o PHP-CLI e o desenvolvimento de aplicações em PHP para a linha de comando, não poderíamos deixar de falar da leitura e escrita nos três principais descritores de arquivos, os quais representam os streams de entrada e saída: `STDIN`, `STDOUT` e `STDERR`.

Para o kernel Linux (e todos os sistemas *Unix like*), tudo é arquivo, inclusive dispositivos físicos, como os nossos teclados, monitores e os fluxos de dados entre o kernel e o usuário. Quando iniciado, o kernel disponibiliza três descritores de arquivos (FD) especiais:

| FD | Nome   | Função                                                                         |
|----|--------|--------------------------------------------------------------------------------|
| 0  | STDIN  | Capturar todo o fluxo de dados digitado no terminal (entrada padrão).          |
| 1  | STDOUT | Enviar fluxos de dados para a exibição no terminal (saída padrão).             |
| 2  | STDERR | Enviar fluxos de dados contendo erros para o terminal (saída padrão de erros). |

O PHP-CLI traz definidas algumas constantes para facilitar a programação envolvendo os fluxos de entrada e saída:

| Constante | Descrição                              |
|-----------|----------------------------------------|
| `STDIN`   | Um stream aberto para a entrada padrão |
| `STDOUT`  | Um stream aberto para a saída padrão   |
| `STDERR`  | Um stream aberto para a saída de erros |

Portanto, é possível ler e escrever nas nossas próprias cópias dos respectivos descritores de arquivos utilizando essas constantes nos nossos códigos. Por exemplo:


### Lendo a entrada padrão

```
echo 'Digite o seu nome: ';
$nome = trim(fgets(STDIN));
printf("Seu nome é %s." $nome);
```

### Escrevendo na saída padrão

```
$msg = "Olá, mundo!\n";
fwrite(STDIN, $msg);
```

### Escrevendo na saída e erros

```
$msg = "Olá, mundo!\n";
fwrite(STDERR, $msg);
```

> **Nota:** Os sistemas *Unix like* replicam fluxos de erro para a saída padrão.

Embora práticas, essas constantes podem apresentar problemas (especialmente `STDIN`) em algumas situações. Observe o exemplo:

```
echo 'Caractere 1: '; 
$a = fgetc(STDIN);

echo 'Caractere 2: ';
$b = fgetc(STDIN); 
```

Quando executado, este código irá pausar a execução para pedir a entrada de um único caractere, o que acontecerá com a chamada da primeira linha `fgetc(STDIN)`. Mas, ao teclarmos `Enter`, nós estamos enviando mais um caractere junto com aquele que digitamos, o que fará com que o `fgetc(STDIN)` seguinte seja "pulado".

Isso acontece porque a constante `STDIN` estará apontando sempre para o mesmo *handler* do descritor de arquivos durante a execução do script:

```
echo 'Caractere 1: '; 
$a = fgetc(STDIN);

echo 'Caractere 2: ';
$b = fgetc(STDIN); 

echo "1: $a\n2: $b\n";
```

Executando...

```
:~$ php streamio-4.php 
Caractere 1: ab
Caractere 2: 
1: a
2: b
```

## 4.8.1 - Abrindo fluxos de entrada e saída com 'php://'

Na verdade, o que as constantes de stream de entrada e saída fazem é retornar um manipulador (*handler*) para o descirtor de arquivos de um fluxo de dados. Elas armazenam o equivalente a...

| Constante | Instrução                   |
|-----------|-----------------------------|
| `STDIN`   | `fopen('php//stdin', 'r')`  |
| `STDOUT`  | `fopen('php//stdout', 'w')` |
| `STDERR`  | `fopen('php//stderr', 'w')` |

Ou seja, um *handler* para o descritor de arquivos é criado, mas não é fechado enquanto o script não terminar a sua execução. Portanto, para evitar surpresas, pode ser mais insteressante realizar todo o processo de abertura e fechamento do descritor de arquivos:

```
function readchar($prompt) {
    $h = fopen('php://stdin', 'r');
    echo $prompt;
    $input = fgetc($h);
    fclose($h);
    return $input;
}


$a = readchar('Caractere 1: ');
$b = readchar('Caractere 2: ');

echo "\n1: $a\n2: $b\n";
```

Executando...

```
:~$ php streamio-5.php 
Caractere 1: ab
Caractere 2: c

1: a
2: c
```

# 5 - Operadores

Os operadores são elementos que pegam uma expressão e geram um valor, de modo que a própria operação torna-se uma expressão. Um exemplo disso é o operador de atribuição `=`, utilizado para atribuir o valor de uma expressão a uma variável:

```
$a = 10;
```

Após esta operação ser executada, tanto `$a` quanto `10` (uma expressão literal de valor 10) e `a = 10` são expressões que passam a representar o valor 10.

Se agruparmos os operadores de acordo com o número de valores envolvidos nas operações que eles realizam, nós veremos que, no PHP, eles podem ser de três tipos:

* **Unários** - Operadores que recebem apenas um valor.
* **Binários** - Quando recebem dois valores. 
* **Ternário** - O único operador do PHP que recebe três valores.

## 5.1 - Prececência e agrupamento de operadores

Quando trabalhamos com vários operadores numa expressão, é fundamental compreender em que ordem o interpretador irá avaliá-los antes de realizar as operações que escrevermos em nossos códigos. Por exemplo:

```
2 + 7 * 2
```

Essa expressão, como aprendemos na matemática, resultará no valor 16, não em 18, porque a multiplicação é efetuada antes da soma, ou seja, a multiplicação tem **precedência** sobre a soma. 

Do mesmo modo, quando dois ou mais operadores de precedência igual aparecem na mesma expressão, o interpretador irá avaliar como eles podem ser agrupados, ou até se eles podem ser agrupados. Outro exemplo, ainda com o operador de adição:

```
2 + 3 + 4
```

Aqui, o operador de adição pode ser agrupado em pares de termos da esquerda para a direita. Portanto, a primeira soma à esquerda será efetuada e seu resultado será somado ao último termo da operação. Já os operadores de atribuição (`=`) são agrupados da direita para a esquerda:

```
$a = $b = 10
```

Neste exemplo, o valor da expressão literal `10` é atribuída à variável `$b` antes do valor da expressão `$b` ser atribuído à variável `$a`.

Uma forma simples de controlar a precedência dos operadores (e poupar dores de cabeça) é através do uso dos parêntesis. Então, voltando ao primeiro exemplo, se quisermos que a soma seja feita antes da multiplicação, nós escrevemos:

```
(2 + 7) * 2
```

O que, desta forma, resultaria em 18.

Contudo, existem tipos de operadores que não podem aparecer juntos na mesma expressão, os chamados operadores não associáveis, como é o caso dos:

* Operadores de comparação;
* Operadores de incremento e decremento;
* Operadores de tipo (casts);
* Operadores ligados à instanciação de novos objetos;
* Operadores de funções geradoras (yield);
* O operador de controle de erro (`@`);
* E do operador de negação lógica (`!`).

São agrupados da direita para a esquerda:

* O operador de potenciação (`**`);
* O operador de coalescência nula (`??`);
* E os operadores de atribuição.

Todos os demais, se forem associáveis, serão agrupados da esquerda para a direita.
> **Importante!** A precedência e a associatividade dos operadores só determinal como as expressões serão agrupadas, mas o PHP geralmente não especifica a ordem exata em que elas serão interpretadas, o que pode nos levar a situações ambíguas, por exemplo:

```
php > $x = 4;
php > echo "4 - 1 = " . $x - 1 . ", ou não?\n";
PHP Notice:  A non well formed numeric value encountered in php shell code on line 1
3, ou não?
```

Repare que, como os operadores `-` e `.` têm a mesma precedência, o PHP efetuou primeiro a concatenação da string `4 - 1 =` com o valor em `$x` para, somente depois, tentar realizar a subtração. Mas, neste ponto, `$x` já não estava disponível como um valor numérico, o que gerou a mensagem de erro.

É por isso que, por menos que seja necessário, é uma ótima ideia utilizar sempre os parêntesis:

```
php > $x = 4;
php > echo "4 - 1 = " . ($x - 1) . ", ou não?\n";
4 - 1 = 3, ou não?
```

## 5.2 - Operadores aritméticos

Os operadores aritméticos são essencialmente os mesmos utilizados nas quatro operações básicas mais o módulo, a potenciação e os sinais de negação e identidade.

| Operador   | Nome          | Resultado |
|:----------:|---------------|-----------|
| `+$a`      | Identidade    | Converte `$a` para um valor do tipo numérico. |
| `-$a`      | Negação       | Converte `$a` para um valor do tipo numérico e inverte o sinal. |
| `$a + $b`  | Soma          | Soma o valor de `$a` com o valor de `$b`. |
| `$a - $b`  | Subtração     | Subtrai de `$a` o valor de `$b`. |
| `$a * $b`  | Multiplicação | Multiplica o valor de `$a` pelo de `$b`. |
| `$a / $b`  | Divisão       | Divide `$a` por `$b`. |
| `$a % $b`  | Módulo        | Resto da divisão de `$a` por `$b`. |
| `$a ** $b` | Potenciação   | Eleva o valor de `$a` à potência `$b`. |

**Observações:**

* Se a divisão resultar em um valor com casas decimais, o quociente será um valor do tipo `float`, a menos que os dois operandos sejam inteiros e divisíveis.

* Para efetuar divisões e ter sempre um inteiro no resultado (obviamente truncado na parte inteira), nós podemos utilizar a função `intdiv()`.

* Os operandos do módulo são truncados e convertidos para inteiros antes da operação ser efetuada. 

* Para obter o módulo de operandos contendo valores do tipo `float`, nós podemos utilizar a função `fmod()`.

* O sinal do módulo será o mesmo do dividendo (o primeiro termo da operação).

## 5.3 - Operadores de atribuição

São os operadores que atribuem ao termo da esquerda o valor da expressão da direita. Como resultado, a própria expressão da atribuição assume o valor atribuído, o que permite fazer coisas como:

```
php > $a = ($b = 5) + 7;
php > echo $a;
12
```

### 5.3.1 - Operadores de atribuição combinados

Além do operador básico de atribuição (`=`), existem operadores combinados para todas as operações aritméticas e os operadores de strings e de união de arrays:

| Operador         | Equivalência            |
|:----------------:|-------------------------|
| `$a += $b`       | `$a = $a + $b`          |
| `$a -= $b`       | `$a = $a - $b`          |
| `$a *= $b`       | `$a = $a * $b`          |
| `$a /= $b`       | `$a = $a / $b`          |
| `$a %= $b`       | `$a = $a % $b`          |
| `$a **= $b`      | `$a = $a ** $b`         |
| `$a .= $b`       | `$a = $a . $b`          |
| `$arr1 += $arr2` | `$arr1 = $arr1 + $arr2` |

### 5.3.2 - Operadores de incremento e decremento

Os operadores de incremento e decremento correspondem aos operadores de atribuição combinados `+=` e `-=` seguidos do valor `1`. Contudo, os operadores de incremento e decremento oferecem uma opção a mais quanto ao momento em que a operação será efetuada: antes ou depois do retorno do valor processado.

| Operador | Nome           | Descrição                                         |
|:--------:|----------------|---------------------------------------------------|
| `$a++`   | Pós incremento | Retorna o valor de `$a` e depois soma 1.          |
| `++$a`   | Pré incremento | Soma 1 ao valor de `$a` e retorna o resultado.    |
| `$a--`   | Pós decremento | Retorna o valor de `$a` e depois subtrai 1.       |
| `--$a`   | Pré decremento | Subtrai 1 do valor de `$a` e retorna o resultado. |

## 5.4 - Operadores bit-a-bit (*bitwise*)

São os operadores que permitem a manipulação e a interpretação de bits específicos de valores inteiros.

| Operador | Nome | Descrição |
|:--------:|------|-----------|
| `$a & $b` | E (*and*) | Bits ativos simultaneamente em `$a` e `$b` são os bits ativos no inteiro resultante. |
| `$a \| $b` | OU (*or*) | Bits ativos em `$a` ou `$b` são os bits ativos no inteiro resultante. |
| `$a ^ $b` | OU exclusivo (*xor*) | Bits ativos em `$a` ou `$b`, mas não em ambos, são os bits ativos no inteiro resultante. |
| `~ $a` | NÃO (not) | Inverte o bits ativos e inativos em `$a`. |
| `$a << $b` | Deslocamento para esquerda | Desloca os bits de `$a` `$b` passos para a esquerda. |
| `$a >> $b` | Deslocamento para direita | Desloca os bits de `$a` `$b` passos para a direita. |

**Observações**

* O deslocamento de bits no PHP é aritmético.

* Somente deslocamentos para a direita preservam o bit de sinal.

* Se ambos os termos das expressões com `&` `|` e `^` forem strings, os operadores irão considerar os valores em ASCII dos caracteres.

* Se o operador `~` for usado com uma string, a operação será executada com os valores ASCII dos caracteres.

* Os operadores `<<` e `>>` sempre tratarão os termos da operação como inteiros.

* Ao utilizar qualquer um desses operadores, devemos nos lembrar de que as operações são realizadas com os bits de valores do tipo inteiro, o que implica na existência de vários bits com estado `0` antes da representação binária dos valores envolvidos.

Por exemplo:

```
php > $v = 5;
php > printf("%b", $v);
101
```

Na conversão para binário, o `printf()` ocultou todos os bits antes do primeiro bit `1`, já que eles estão inativos (`0`). Mas, se utilizarmos o operador NÃO (`~`)...

```
php > printf("%b", ~$v);
1111111111111111111111111111111111111111111111111111111111111010
```

Como podemos ver, todos os bits inativos do inteiro `5` foram ativados e vice-versa, o que resultaria no inteiro `-6`, e não no decimal `10`. Isso acontecepor dois motivos:

1. O PHP não trabalha com inteiros sem sinal.
2. Binários iniciados com bit 1 representam valores negativos.

## 5.5 - Operadores de comparação

São os operadores utilizados para comparar dois valores e gerar um valor verdadeiro (`true`) ou falso (`false`) de acordo com a verdade da afirmação feita na expressão.

| Operador | Afirmação | Descrição |
|:----------:|------|-----------|
| `$a == $b` | É igual | Verdadeiro se `$a` for igual a `$b` após o ajuste de seus tipos. |
| `$a === $b` | É idêntico | Verdadeiro se `$a` for estritamente igual a `$b`, inclusive no tipo. |
| `$a != $b` | É diferente | Verdadeiro se `$a` for diferente de `$b` após o ajuste de seus tipos. |
| `$a <> $b` | É diferente | Verdadeiro se `$a` for diferente de `$b` após o ajuste de seus tipos. |
| `$a !== $b` | Não é idêntico | Verdadeiro se `$a` for diferente de `$b` ou se seus valores forem de tipos diferentes. |
| `$a < $b` | É menor | Verdadeiro se `$a` for menor do que `$b`. |
| `$a > $b` | É maior | Verdadeiro se `$a` for maior do que `$b`. |
| `$a <= $b` | É menor ou igual | Verdadeiro se `$a` for menor ou igual a `$b`. |
| `$a >= $b` | É maior ou igual | Verdadeiro se `$a` for maior ou igual a `$b`. |
| `$a <=> $b` | "Nave espacial" | Retorna um inteiro maior, menor ou igual a zero se `$a` for respectivamente maior, menor ou igual a `$b`. |

### 5.5.1 - O operador ternário

Um tipo especial de operador de comparação é o **operador ternário**, que funciona mais ou menos como uma forma abreviada da declaração `if`.

```
(EXPRESSÃO1) ? (EXPRESSÃO2) : (EXPRESSÃO3)
```

Aqui, EXPRESSÃO2 será efetuada se a EXPRESSÃO1 for verdadeira. Caso seja falsa, a EXPRESSÃO3 é que será efetuada. Por exemplo:

```
php > $a = 1;
php > $b = ($a > 5) ? 5 : $a++;
php > echo $b;
1
```

Alternativamente, podemos ocultar a expressão do meio:

```
(EXPRESSÃO1) ?: (EXPRESSÃO3)
```

Assim, se EXPRESSÃO1 for verdadeira, seu valor será o resultado do ternário. Caso contrário, será o valor de EXPRESSÃO3:

```
php > $a = 3;
php > $b = ($a > 5) ?: $a++;
php > var_dump($b);
int(3)
php > $a = 10;
php > $b = ($a > 5) ?: $a++;
php > var_dump($b);
bool(true)
```

### 5.5.2 - O operador de coalescência nula

A partir do PHP 7, nós temos mais um operador muito interessante, com o qual nós podemos atribuir um valor padrão a uma variável: `??`, o operador de coalescência nula.

```
(EXPRESSÃO1) ?? (EXPRESSÃO2)
```

Aqui, a EXPRESSÃO2 será efetuada se a EXPRESSÃO1 for nula, o que pode ser muito útil para a definição de valores alternativos, caso o elemento de uma array não esteja definido:

```
php > $par = $a[1] ?? "Não tem...";
php > echo $par;
Não tem...
```

Ou então, em um script (`teste`)...

```
#!/usr/bin/env php
<?php

function erro() {
    echo "Você errou!\n";
    exit(25);
}

$a = $argv[1] ?? erro();

echo "$a - Tudo certo!\n";
```

Executando...

```
:~/cursos/php-cli/09$ ./teste 
Você errou!
:~/cursos/php-cli/09$ ./teste olá
olá - Tudo certo!
```

## 5.6 - Operadores lógicos

Diferente dos operadores de comparação, que avaliam afirmações sobre as expressões e retornam o valor verdade resultante, os operadores lógicos geram um valor verdadeiro (`true`) ou falso (`false`) de acordo com a condição de verdade de cada termo da expressão. Ou seja, eles irão comparar o estado lógico de cada expressão participante, não a correspondência entre elas.

| Operador     | Nome      | Descrição                                                             |
|:------------:|-----------|-----------------------------------------------------------------------|
| `$a and $b`  | E (*and*) | Retorna `true` se `$a` e `$b` forem `true`.                           |
| `$a or $b`   | OU (*or*) | Retorna `true` se `$a` ou `$b` forem `true`.                          |
| `$a xor $b`  | OU exclusivo (*xor*) | Retorna `true` se `$a` ou `$b` forem `true, mas não ambos. |
| `! $a`       | NÃO (not) | Retorna `true` se `$a` for `false`.                                   |
| `$a && $b`   | E (*and*) | Retorna `true` se `$a` e `$b` forem `true`.                           |
| `$a \|\| $b` | OU (*or*) | Retorna `true` se `$a` ou `$b` forem `true`.                          |

> **Observação:** Os operadores `||` e `&&` têm precedência sobre seus equivalentes `or` e `and`.


## 5.7 - Operador de execução

Especialmente interessante para nós, que estamos estudando o PHP-CLI, é o operador de execução, que permite executarmos comandos do shell e atribuirmos suas saídas a variáveis. 

```
`COMANDOS DO SHELL`
```

O funcionamento do operador de execução é idêntico ao da função `shell_exec()`.

Exemplo:

```
php > $user = `whoami`;
php > echo $user;
gda
```

# 6 - Estruturas de controle de fluxo

Como vimos no tópico anterior, alguns operadores podem se comportar como estruturas de decisão. É o caso do **operador ternário** e do **operador de coalescência nula** que, de acordo com uma condição, são capazes de realizar atribuições de valores:

**Operador ternário**

```
$VAR = (CONDIÇÃO) ? VALOR_SE_TRUE : VALOR_SE_FALSE;

$VAR = (CONDIÇÃO) ?: VALOR_SE_FALSE;
```

**Operador de coalescência nula**

```
$VAR = (EXPRESSÃO1) ?? (EXPRESSÃO_SE_EXPRESSÃO1_FOR_NULA);
```

As estruturas de controle de fluxo fazem a mesma coisa, só que com blocos inteiros de instruções de todo tipo: atribuições, funções, *loops*, declarações condicionais, etc... O agrupamento de instruções no PHP é feito através das chaves (`{...}`). O mais importante aqui, é que todo bloco de instruções agrupado entre chaves também é uma instrução.

No PHP, nós poderíamos classificar esses blocos em três grupos principais:

* Estruturas de decisão
* Estrututras de repetição
* Operador de salto (goto)

Todas essas estruturas são construtoras de linguagem, todas podem anteceder grupos de instruções entre chaves e, exceto no caso do operador `goto`, todas as demais podem ser chamadas de **declarações** ou, como é mais comum e preciso em português, **cláusulas** (pense em "cláusulas de um contrato").

## 6.1 - As cláusulas 'if/elseif/else'

Presente em diversas linguagens, a cláusula `if` permite a execução condicional de blocos de código. Na sua forma básica, uma estrutura `if` seria escrita desta forma:

```
if (EXPRESSÃO_TRUE) INSTRUÇÃO;
```

Onde EXPRESSÃO_TRUE é qualquer expressão avaliada como verdadeira. Caso a expressão resulte em `false`, a INSTRUÇÃO é ignorada. Por exemplo:

```
php > $a = 10;
php > if ($a < 15) echo "É menor do que 15!";
É menor do que 15!
```

Ou então:

```
php > $a = 20;
php > if ($a < 15) echo "É menor do que 15!";
php > 
```

Onde, neste caso, a instrução foi ignorada.

Observe, porém, que esta cláusula `if` está condicionando a execução de apenas uma instrução. Tudo que vier depois dela no código, se ainda houver algo a ser executado, não está condicionado à expressão avaliada:

```
php > $a = 10;
php > if ($a < 15) echo "É menor!\n"; echo "E a vida segue...";
É menor!
E a vida segue...

php > $a = 20;
php > if ($a < 15) echo "É menor!\n"; echo "E a vida segue...";
E a vida segue...
```

Neste exemplo, a segunda instrução `echo` foi executada de forma independente da condição estabelecida pelo `if`. Se a ideia fosse condicionar a instrução seguinte à avaliação da mesma condição, digamos, para executar algo caso a expressão seja avaliada como falsa, nós poderíamos utilizar a instrução `else`:

```
php > // A expressão é verdadeira...
php > $a = 10;
php > if ($a < 15) echo "Menor!\n"; else echo "Maior!\n"; echo "Seguindo...";
Menor!
Seguindo...

php > // A expressão é falsa...
php > $a = 20;
php > if ($a < 15) echo "Menor!\n"; else echo "Maior!\n"; echo "Seguindo...";
Maior!
Seguindo...
```

A instrução `else` depende da ocorrência de uma cláusula `if` (ou `elseif`, como veremos adiante) imediatamente antes, ou não será executada. O exemplo abaixo resultaria em um erro de sintaxe:

```
php > else echo "Foi?";
PHP Parse error:  syntax error, unexpected 'else' (...)
```

A instrução `else` também pode vir acompanhada de uma segunda condição declarada com um `if`, resultando numa instrução `else if` ou, de forma abreviada, `elseif`:

```
php > // A expressão em 'else if' é a verdadeira...
php > $a = 15;
php > if ($a < 10) echo "Menor\n";
php > else if ($a > 10) echo "Maior\n"; 
Maior
php > else echo "Igual\n";
php > 
```

### 6.1.1 - Agrupando instruções

Embora essa sintaxe seja muito prática para condicionarmos a execução de apenas uma instrução a cada uma das cláusulas `if`, `elseif` e `else`, é bastante comum nós precisarmos executar um grupo de várias instruções, e é aí que entram as chaves.

Talvez a sintaxe mais conhecida de uma estrutura `if/elseif/else` seja aquela com as instruções agrupadas entre chaves, mas isso só é realmente obrigatório quando precisamos desses agrupamentos:

```
if (EXPRESSÃO_TRUE 1) {

    INSTRUÇÕES...

} elseif (EXPRESSÃO_TRUE 2) {

    INSTRUÇÕES...

} elseif (EXPRESSÃO_TRUE N) {

    INSTRUÇÕES...

} else {

    INSTRUÇÕES...

}
```

### 6.1.2 - Uma sintaxe alternativa

Uma sintaxe alternativa pouco conhecida é aquela que substitui a abertura das chaves por dois pontos (`:`) e o fechamento do bloco por um `endif;`:

```
if (EXPRESSÃO_TRUE 1):

    INSTRUÇÕES...

elseif (EXPRESSÃO_TRUE 2):

    INSTRUÇÕES...

elseif (EXPRESSÃO_TRUE N):

    INSTRUÇÕES...

else:

    INSTRUÇÕES...

endif;
```

> **Importante!** com esta sintaxe, o `elseif` não pode ser utilizado na sua forma separada (`else if`)!

Apesar de lembrar as estruturas na linguagem Python, observe que aqui as tabulações são meramente estéticas, tanto que isso é possível:

```
php > $a  = true;
php > if ($a): echo "bom\n"; echo "dia\n"; else: echo "falso\n"; endif;
bom
dia
```

### 6.1.3 - Uma dica especial

Esta é uma forma simples e prática de exibir mensagens condicionada à uma variável estar ou não definida:

```
php > // $c não está definida...
php > isset($c) and print $c;

php > // Definindo $c...
php > $c = 23;
php > isset($c) && print $c;
23
```

Ou então...

```
php > $a = 15;
php > // $c não está definido...
php > isset($c) || print $a;
15
```

Ou, brincando mais um pouco com as expressões condicionais...

```
php > $a = 15;
php > // $c não está definido...
php > (isset($c) && print $c) || print $a;
15

php > // Definindo $c...
php > $c = 23;
php > (isset($c) && print $c) || print $a;
23
```

Se não quisermos usar parêntesis, nós podemos jogar com as precedências:

```
php > $a = 15;
php > // $c não está definido...
php > isset($c) && print $c or print $a;
15

php > // Definindo $c...
php > $c = 23;
php > isset($c) && print $c or print $a;
23
```

O princípio do funcionamento dessas expressões, apesar de parecer um mistério, é bem simples:

* Em uma operação lógica com o operador `&&` (ou `and`), se o primeiro termo é avaliado como `false` o valor de toda a expressão já está definido como `false`, e o segundo termo não precisa ser avaliado. Portanto, o `print` não chega a ser executado.

* Já numa operação lógica com o operador `||` (ou `or`), mesmo que o primeiro termo seja avaliado como `false`, a avaliação da expressão ainda dependerá do valor do segundo termo e, por isso, o `print` precisa ser executado. Por outro lado, se o primeiro termo for avaliado como `true`, a expressão inteira terá valor `true`, e o segundo termo não será avaliado.

## 6.2 - A estrutura 'switch/case'

De modo geral, o `switch` é similar a um conjunto de declarações `if/elesif` encadeadas onde uma mesma expressão é comparada com diversos valores:

```
if ($a == 1) {
    INSTRUÇÕES
} elseif ($a == 2) {
    INSTRUÇÕES
} elseif ($a == 3) {
    INSTRUÇÕES
} else {
    INSTRUÇÕES
}
```

Com a estrutura `switch/case`, o mesmo resultado seria obtido da seguinte forma:

```
switch($a) {
    case 1:
        INSTRUÇÕES
        break;
    case 2:
        INSTRUÇÕES
        break;
    case 3:
        INSTRUÇÕES
        break;
    default:
        INSTRUÇÕES
}
```

> **Nota:** A estrutura `switch/case` também suporta a forma alternativa sem chaves. Neste caso, o bloco deve ser encerrado com a palavra-chave `endswitch`.

A diferença entre as duas estruturas é que, no `switch`, a expressão é avaliada apenas uma vez, o que torna perfeitamente seguro avaliar o retorno de funções (internas ou do usuário) sem a preocupação com o fato delas terem que ser chamadas mais de uma vez. Obviamente, isso afeta positivamente o desempenho dos nossos programas.

Repare que, no equivalente `if/elseif`, nós utilizamos o operador `==`, de comparação flexível (que não considera os tipos). Isso porque o `switch` faz exatamente esse tipo de comparação, ou seja, o inteiro `0`, as strings e arrays vazias e o valor `false` serão tratados como equivalentes na comparação.

A cláusula `default`, ainda no exemplo acima, é um tipo especial de `case` que casa com qualquer valor da expressão em `switch`. Ela não é obrigatória, mas deve ser sempre utilizada como a última cláusula da estrutura.

É muito importante entender que a estrutura `switch/case` **executa linha por linha cada uma das instruções**. No começo, nenhum código é executado, o que acontece apenas quando uma cláusula `case` expressa um valor que encontra correspondência com o valor da expressão na cláusula `switch`. Neste ponto, e a partir daí, **todas as instruções subsequentes são executadas até o fim do bloco `switch`!**

Por este motivo, nós somos obrigados a utilizar a instrução `break` para interromper a execução do bloco `switch` no ponto em que queremos que isso conteça. Pode ser na própria cláusula `case` que encontrou correspondência ou em algum trecho mais adiante (depende do que queremos fazer), mas o `break` é a única forma de impedir a continuidade da execução. Por exemplo:

**Exemplo `sw01.php`**

```
$a = 1;

switch($a) {
    case 1:
        echo "1\n";
    case 2:
        echo "2\n";
    case 3:
        echo "3\n";
    default:
        echo "qualquer coisa!\n";
}
```

Executando, a nossa saída seria:

```
:~$ php sw01.php 
1
2
3
qualquer coisa!
```

Portanto, se esse comportamento não for o desejado, não podemos nos esquecer da instrução `break` ao final de cada bloco `case`.

Por outro lado, nós podemos aproveitar esse comportamento para definir mais de uma correspondência válida:

**Exemplo `sw02.php`

```
switch($argv[1]) {
    case 0:
    case 1:
    case 2:
        echo "0, 1 ou 2\n";
        break;
    case 3:
        echo "3\n";
        break;
    default:
        echo "qualquer coisa!\n";
}
```

Aqui, nosso programa executaria as instruções em `case 2:` mesmo que o usuário fornecesse os valores `0` ou `1`:

```
:~$ php sw02.php 0
0, 1 ou 2
:~$ php sw02.php 1
0, 1 ou 2
:~$ php sw02.php 2
0, 1 ou 2
:~$ php sw02.php 3
3
:~$ php sw02.php 4
qualquer coisa!
```

> **Atenção!** Apesar de também ser uma estrutura de decisão, no PHP a cláusula `switch/case` é considerada uma estrutura de repetição. Então, intruções como `break` e `continue` irão afetar apenas o contexto do próprio bloco `switch`. Isso ajuda muito a entender as peculiaridades do `switch` em comparação aos blocos `case` de outras linguagens e é especialmente importante quando um `switch` for utilizado dentro de uma estrutura de repetição (como um `while`, por exemplo).

Outro ponto importante sobre as cláusulas `case`, é que elas também podem receber expressões em vez de valores literais. Isso poderia resolver o fato das comparações serem feitas de forma flexível (`==`):

**Exemplo sw03.php**

```
$a = false;

switch (true) {
    case ($a === 0):
        echo "O valor é zero.\n";
        break;
    case ($a === ""):
        echo "A string é vazia.\n";
        break;
    case ($a === false):
        echo "O valor é FALSE!\n";
        break;
}
```

Neste exemplo, a expressão em `switch` é o valor booleano literal `true`, mas ele não está sendo comparado com nenhum dos valores dos blocos `case`. A comparação aqui é feita em cada uma das cláusulas, onde nós utilizamos o operador `===` para garantir que `$a` fosse comparado de forma estrita (levando em conta os tipos) com os valores em questão, todos eles interpretados normalmente como `false` pelo PHP.

> **Um questionamento:** Realmente, é uma solução, funciona. Contudo, será que não deveríamos optar por outra estrutura (`if/elseif`) neste caso? Afinal, toda a vantagem do `switch` sobre o seu equivalente implementado com `if/elseif` é o fato dele avaliar a expressão apenas uma vez.

## 6.3 - O operador 'goto'

O operador `goto` é utilizado para fazer o fluxo de execução saltar de um ponto para outro no código. O ponto de destino é definido com um *label* (rótulo) seguido de dois pontos:

```
goto a;

echo "Não tá me vendo aqui?!\n";

a:

echo "A fila andou, meu cara!\n"
```

A implementação do `goto` no PHP não é irrestrita e deve seguir algumas regras:

* O rótulo não pode ser uma variável.
* O rótulo de destino tem que estar no mesmo arquivo e no mesmo contexto do `goto`.
* Portanto, não podemos saltar para dentro do contexto de uma função, por exemplo, e nem sair dela!
* Também não podemos saltar para o interior de uma estrutura de repetição (inclusive um `switch`, nesse sentido).
* Mas podemos utilizar o `goto` para sair de loops e do `switch`.

Por outro lado, nós podemos agrupar blocos de instruções com chaves e identificá-los com rótulos, mas isso não impede que as instruções sejam executadas!

```
a: {
    echo "banana\n";
    echo "laranja\n";
    goto b;
}

echo "Isso não será lido!\n";

b:

exit(0);

```

Repare que o label `a:` nunca foi chamado, mas ainda teríamos na saída....

```
banana
laranja
```

As possibilidades do `goto` são muitas, e ele pode ser muito útil, especialmente para sairmos do switch ou de um loop (ou de ambos aninhados) sem utilizarmos vários `break`:

**Exemplo `sw04.php`**

```
if (!isset($argv[1])) goto d;

switch($argv[1]) {
    case 1:
        goto a;
    case 2:
        goto b;
    default:
        goto c;
}

a: {
    echo "banana\n";
    echo "laranja\n";
    goto c;
}

b: {
    echo "limão\n";
    echo "abacaxi\n";
    goto c;
}


c:

exit(0);

d:

echo "Nenhum parâmetro passado!\n";

goto c;
```

Também é possível implementar um loop com o operador `goto`, o que deve ser usado com cautela -- se não houver uma condição de saída mandando o fluxo de execução para outro ponto no código, o loop será infinito. Por exemplo, isso causaria um loop infinito:

```
$a=0;
a:
echo ++$a.PHP_EOL;
goto a;
```

Com alguma condição de saída, nós podemos controlar o loop, mas só podemos utilizar outro `goto`para isso:

```
$a=0;
a:
echo ++$a.PHP_EOL;
if ($a == 10) goto b;
goto a;
b:
```

## 6.4 - O loop 'for'

Apesar de ter sido bastante utilizado em exemplos anteriores, chegou a hora de conhecer um pouco melhor o loop `for`. No PHP, ele se comporta como seu equivalente na linguagem C, inclusive na sintaxe:

```
for (EXPRESSÃO1; EXPRESSÃO2; EXPRESSÃO3) {
    INSTRUÇÕES
}
```

No começo do loop, a EXPRESSÃO1 é executada uma vez incondicionalmente. Antes de cada iteração EXPRESSÃO2 é avaliada, e o bloco de instruções só é executado se a avaliação resultar em `true`. Caso contrário, o loop é encerrado. A EXPRESSÃO3, por sua vez, só é executada no final de cada iteração.

Exemplo:

```
for ($n = 0; $n < 10; $n++) {
    echo $n;
}
```

* **1**: `$n` recebe o valor `0`;
* **2**: A expressão `$n < 10` é avaliada;
* **3**: Sendo `true`, a instrução `echo $n` é executada;
* **4**: O valor de `$n` passa a ser `1`;
* **5**: A expressão `$n < 10` é avaliada novamente;

E o ciclo continua até que o valor da expressão `$n < 10` seja falso.

Além disso, qualquer uma das 3 expressões pode conter várias expressões separadas por vírgula ou pode até ser omitida, permitindo que o loop `for` emule o comprtamento de outras estruturas e conjuntos de instruções:

**Exemplo 1 - sem a primeira expressão:**

```
php > // Observe que o ';' ainda precisa ser escrito! 
php > $n = 1;
php > for (; $n <= 10; $n++) { echo "$n "; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 2 - sem a segunda expressão (loop infinito):**

```
php > // Observe a instrução de saída `break'...
php > for ($n =1;;$n++) { echo "$n "; if ($n == 10) break; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 3 - sem a terceira expressão:**

```
php > // Observe o incremento na última instrução...
php > for ($n =1;$n <= 10;) { echo "$n "; $n++; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 4 - sem as expressões (outro loop infinito):**

```
php > $n = 1;
php > for (;;) { echo "$n "; $n++; if ($n > 10) break; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 5 - sem o bloco de instruções:**

```
php > for ($x=0, $y=1; $x < 10; $x+=$y, print "$x ");
1 2 3 4 5 6 7 8 9 10 
```

O loop for também aceita a sintaxe alternativa sem chaves utilizando o delimitador `endfor;`:

```
for (EXPRESSÃO1; EXPRESSÃO2; EXPRESSÃO3):
    INSTRUÇÕES
endfor;
```

**Importante!** Após qualquer loop, as variáveis utilizadas para controlar o fim da iteração continuarão disponíveis e com o valor recebido na última iteração:


```
php > for ($n = 1; $n <= 10; $n++) { echo "$n "; }
1 2 3 4 5 6 7 8 9 10
php > echo $n;
11
```

## 6.5 - O loop 'while'

Uma estrutura de repetição bem mais simples (na verdade, a mais simples de todas) é o loop `while`, que também se comporta como o seu equivalente em C:

```
while (EXPRESSÃO) {
    INSTRUÇÕES
}
```

O `while` faz com que o conjunto de instruções seja executado **enquanto** (*while*) EXPRESSÃO for avaliada como verdadeira, o que é testado sempre no início de cada ciclo de iteração. Portanto, mesmo que o valor de EXPRESSÃO mude para `false` durante a execução do bloco de instruções, o loop só será interrompido após o fim da iteração corrente.

**Nota:** EXPRESSÃO tem que ser avaliada como `true` para o loop poder começar.

Nós podemos implementar um loop `while` infinito com qualquer expressão que retorne sempre verdadeiro, inclusive com o valor booleano `true`.

**Exemplo 1:**

```
php > $n = 1;
php > while ($n <= 10) { echo "$n "; $n++; }
1 2 3 4 5 6 7 8 9 10
```

**Exemplo 2 (loop infinito):**

```
php > $n = 1;
php > while (true) { echo "$n "; $n++; if ($n > 10) break; }
1 2 3 4 5 6 7 8 9 10
```

O loop `while` também aceita a sintaxe alternativa sem chaves e com o delimitador `endwhile;`:

```
while (EXPRESSÃO):
    INSTRUÇÕES
endwhile;
```

## 6.6 - O loop 'do/while'

O loop `do/while` é idêntico ao loop `while` com a diferença do momento da avaliação de EXPRESSÃO, que é feita ao final da iteração.

```
do {
    INSTRUÇÕES
} while (EXPRESSÃO);
```

Como consequência, pelo menos a primeira iteração sempre irá ocorrer.

Essa característica era muito usada antes do PHP 5.3 (na verdade, ainda é) para encapsular um trecho do código, interromper o loop `do/while` e saltar para as instruções seguintes. Hoje, após a implementação do operador `goto`, isso deixou de ser necessário:

**Exemplo com `do/while`**

```
// $a = 1;
do {

    if ($a > 5) break;

    // OUTRAS INSTRUÇÕES

} while (false);

// Segue o código...
```

**Exemplo com `goto`**

```
// $a = 1
if ($a > 5) goto segue;

// OUTRAS INSTRUÇÕES

segue:

// Segue o código...

```

## 6.7 - Interrompendo loops

Como já vimos, a instrução `break` interrompe a execução das estruturas:

* `for`
* `foreach`
* `while`
* `do/while`
* `switch`

O que não vimos, porém, é que o `break` aceita um argumento numérico opcional que permite especificar o nível da estrutura aninhada que deve ser interrompida. Se não for informado, o nível será sempre `1`, o que corresponde à estrutura em que o `break` está sendo executado. Por exemplo:

```
while (true) {
    $r = readline("Digite algo: ");
    switch($r) {
        case 1:
            $opt = 'banana';
            break;
        case 2:
            $opt = 'laranja';
            break;
        default:
            echo "Nenhuma escolha foi feita.\n";
            break 2;
    }
    echo "A opção foi: $opt\n";
}
```

Aqui, se o usuário digitar `1` ou `2`, a variável `$opt` receberá um valor e o `break` (sem argumento) fará com que o `switch` seja interrompido e a instrução seguinte seja executada. Mas, se o usuário digitar qualquer outra coisa, uma mensagem é exibida e todo o loop `while` infinito é interrompido com a instrução `break 2`.

**Nota:** Desde o PHP 5.4 não é mais possível passar o argumento numérico do `break` por uma variável (`break $nivel`).

## 6.8 - Saltando o restante da iteração

Outra instrução bastante utilizada para controlar estruturas de loop é o `continue`, que faz com que todas as instruções subsequentes sejam ignoradas e a próxima iteração seja executada:

```
for ($n = 0; $n <= 10; $n++) {
    if (($n % 2) == 0) continue;
    echo "$n ";
}
```

O exemplo irá exibir apenas os números ímpares entre `0` e `10`, já que o `continue` impedirá a execução da instrução `echo "$n "` sempre que o módulo da divisão de `$n` por `2` for `0`:

```
1 3 5 7 9
```

Tal como o `break`, o `continue` também aceita um argumento numérico para especificar quantos níveis de estruturas aninhadas devem ser saltadas.

# 7 - Funções

Uma função é um conjunto de instruções agrupadas de forma a poderem ser invocadas de qualquer parte do código para executarem um procedimento e/ou retornarem algum valor. Até certo ponto, nós podemos entender as funções no PHP como "programas dentro dos nossos programas" (daí também serem chamadas de "sub-rotinas"):

* Elas geralmente têm um nome pelo qual podem ser chamadas;
* Podem aceitar parâmetros de execução;
* Podem retornar valores;
* Podem exibir saídas;
* Podem capturar entradas;
* Podem manipular arquivos...

Enfim, tal como os nossos programas em PHP.

A única diferença é que as funções são pensadas para executarem procedimentos  muito mais específicos (daí também serem chamadas de "métodos") que precisaríamos executar várias vezes nos nossos códigos.

Como já vimos nos primeiros tópicos, o PHP fornece uma infinidade de funções prontas para serem utilizadas nos nossos programas, as funções *built-in*, mas também nos dá a possibilidade de criarmos as nossas próprias, e é disso que falaremos neste tópico.

## 7.1 - Por que criar funções

Os motivos são muito bons:

* **Organização:** com as funções, nós podemos separar cada trecho do nosso código conforme a tarefa que executa, o que torna a manutenção muito mais fácil.

* **Reutilização:** basta escrevermos uma tarefa repetitiva uma única vez para chamá-la quantas vezes quisermos.

* **Modularidade:** conjuntos de funções permitem que os nossos códigos sejam vistos como a soma de vários pequenos módulos especializados em uma tarefa, como blocos de um Lego que podemos montar conforme a necessidade.

* **Bibliotecas pessoais:** com o tempo, muitas das nossas funções podem acabar solucionando problemas que encontraremos em vários projetos, o que permite até a criação de bibliotecas pessoais ou pequenos *frameworks* com o código reaproveitado.

## 7.2 - Anatomia de uma função

No PHP, as funções são criadas com a palavra-chave `function` seguida de um nome, uma lista de parâmetros de execução entre parêntesis e um agrupamento de instruções entre chaves:

```
function NOME([par_1, par_2, ... , par_n]) {
    INSTRUÇÕES...
    return VALOR;
}
```

### Nome da função

O nome da função segue as mesmas regras de outros identificadores do PHP: deve começar com uma letra ou com o caractere sublinhado seguido de qualquer quantidade de letras, números e sublinhados.

### Lista de parâmetros

As funções no PHP podem receber zero ou mais parâmetros, que são uma forma de passarmos os dados que queremos elas processem. A lista de parâmetros consiste de zero ou mais variáveis separadas por vírgulas, com ou sem valores atribuídos por padrão.

Exemplo:

```
function soma($a, $b) {
    $c = $a + $b;
    return $c;
}
```

Quando chamada, a função `soma()` vai exigir que os valores de `$a` e `$b` sejam informados, o que deve ser feito respeitando o tipo de dado exigido e a ordem em que os parâmetros aparecem na declaração da função. Se houver mais de um, como no exemplo, eles devem ser separados por vírgula:

```
soma(4, 3);
     ^  ^
     |  |
    $a $b
```

Como vimos no tópico "Escopo de variáveis", todas as variáveis definidas dentro das funções, inclusive aquelas que serão utilizadas como parâmetros, têm escopo restrito apenas ao contexto da função, ou seja, não são visíveis de outras partes do código.

> **Nota:** lembre-se de que o contrário também é verdadeiro -- as variáveis do escopo global também não são vistas (por padrão) dentro de funções.

Se a função não receber parâmetros, ela deve ser chamada com os parêntesis vazios:

```
minha_funcao();
```

### Grupo de instruções

As chaves delimitam o conjunto de instruções que serão executadas pelas funções, e elas podem ser qualquer código válido em PHP, inclusive outras funções que nós criarmos.

### Instrução 'return'

As funções podem "devolver" os dados que ela processar de várias formas: alterando uma ou mais variáveis globais, exibindo algo na saída, encerrando a execução do programa com um código de erro, etc. Mas a forma padrão de retornar valores é através da instrução opcional `return`.

```
return [EXPRESSÃO];
```

O `return` faz com que a função termine a sua execução imediatamente, devolvendo o controle ao código na linha em que ela tiver sido chamada. Seu argumento opcional é o valor de uma EXPRESSÃO, e é isso que será retornado para o código. Se a instrução `return` for utilizada sem argumentos, o valor retornado pela função será `NULL`. 

> **Nota:** a instrução `return` também pode ser chamada do corpo principal do código, causando o fim da execução do script.

## 7.3 - Como funcionam as funções

É muito importante entender que uma função, quando chamada, faz com que o script seja pausado até que ela devolva o controle ao fluxo principal do código no ponto em que a invocação aconteceu, retornando o valor de uma expressão ou `NULL`:

**Função com retorno nulo:**

```
php > function teste() { echo "Isso é um teste!\n"; }
php > var_dump(teste());
Isso é um teste!
NULL
```

**Função com o retorno do valor de uma expressão**

```
php > function soma($a, $b) { return $a + $b; }
php > var_dump(soma(2,3));
int(5)
```

Outro ponto importante, é que as funções podem ser chamadas de qualquer ponto no código, inclusive de uma linha anterior à da sua declaração:

**Exemplo (func01.php):**

```
echo soma(2,3)."\n";

function soma($a, $b) {
    return $a + $b;
}
```

Executando...

```
:~$ php func01.php 
5
```

Contudo, nós podemos condicionar a declaração de uma função para que ela só esteja disponível caso alguma condição seja atendida:

**Exemplo (func02.php):**

```
if (isset($argv[1])) {
    function soma($a, $b) {
        return $a + $b;
    }
}

if (function_exists("soma")) {
    echo soma(3,4)."\n";
} else {
    echo "Função não existe!\n";
}
```

Executando...

```
:~$ php func02.php
Função não existe!

:~$ func02.php teste
7
```

Neste caso, a chamada da função só pode ocorrer como no exemplo acima, **depois do ponto em que a função é declarada**. O exemplo abaixo faria `function_exists()` retornar sempre `false`, mesmo que um argumento fosse passado para o script:


```
if (function_exists("soma")) {
    echo soma(3,4)."\n";
} else {
    echo "Função não existe!\n";
}

if (isset($argv[1])) {
    function soma($a, $b) {
        return $a + $b;
    }
}
```

Outra forma de condicionar a disponibilidade de uma função é criá-la dentro de outra função.

**Exemplo (func03.php):**

```
if (!function_exists('existo')) {
    echo "Se não pensar, eu não existo!\n"
}

penso();

function penso() {

    echo "Penso...\n";

    function existo() {
        echo "logo existo!\n";
    }
}

existo();
```

## 7.4 - Funções variáveis

O PHP tem o conceito de **funções variáveis**, ou seja, se uma variável aparecer seguida de parêntesis, o interpretador irá procurar por uma função cujo nome seja o valor da variável e a executará.

**Exemplo (func04.php):**

```
function penso() {
    global $func2;

    echo "Penso, ";
    $func2();
}

function existo() {
    echo "logo existo.\n";
}

$func1 = 'penso';
$func2 = 'existo';

$func1();
```

## 7.5 - Funções anônimas

O PHP também permite a criação de funções anônimas, também chamadas de *closures*. Embora apareçam mais como parâmetros de callback de outras funções, elas têm muitos outros usos interessantes. Por exemplo, nós podemos atribuir funções a variáveis:

```
php > $salve = function($nome) { echo "Olá, $nome!\n"; };
php > $salve('Blau');
Olá, Blau!
```

> **Importante!** A atribuição de funções a variáveis deve terminar com `;`!

## 7.6 - Parâmetros obrigatórios e opcionais

Como dissemos, os parâmetros de uma função podem ser obrigatórios ou opcionais. Por padrão, isso depende de como nós definimos as variáveis na lista de parâmetros: 

```
function NOME($OBRIGATÓRIO, $OPCIONAL=VALOR_PADRÃO) { ... }
```

Se a variável for iniciada com um valor padrão, o parâmetro será opcional.

Isso é muito útil, mas pode trazer alguns problemas quando tivermos mais de um parâmetro opcional:

```
php > function soma($a = 5, $b = 4) { return $a + $b; }
```

Neste exemplo, nós só temos a opção de não passarmos nenhum parâmetro ou não passarmos o segundo (`$b = 4`):

```
php > echo soma();
9
php > echo soma(10);
14
php > echo soma(6,9);
15
```

Isso acontece porque os parâmetros das funções são sempre posicionais, ou seja, eles estão amarrados à ordem em que aparecem na declaração da função, o que acaba limitando a utilidade do recurso a quando temos apenas um parâmetro opcional (geralmente definido por último na lista).

## 7.7 - Passagem de argumentos por valor e por referência

Por padrão, os argumentos são passados por valor, o que faz com que os valores dos argumentos sejam alterados apenas dentro das funções. Para alterar argumentos fora das funções, nós podemos passá-los por referência, o que é feito incluindo o prefixo `&` ao nome da variável na lista de parâmetros que será recebida por referência. Por exemplo:

```
php > function fruta(&$str) { $str .= ' laranja'; }
php > $a = 'banana';
php > fruta($a);
php > echo $a;
banana laranja
```

## 7.8 - Número variável de parâmetros

A partir do PHP 5.6, é possível criar funções que aceitam um número variável de parâmetros prefixando a variável que receberá os argumentos com `...`, por exemplo:

```
php > function soma(...$valores) { return array_sum($valores); }
php > echo soma(1,2,3);
6
```

Isso é o equivalente (menos trabalhoso) de fazer:

```
php > function soma($valores=[]) { return array_sum($valores); }
php > echo soma([1,2,3]);
6
```

Ou então...

```
php > function soma() { return array_sum(func_get_args()); }
php > echo soma(1,2,3);
6
```

# 8 - Funções geradoras

Uma função geradora permite a criação de um conjunto de dados sem a necessidade de carregar uma array, o que pode, em alguns casos, sobrecarregar a memória e os recursos de processamento disponíveis.

O comportamento é o mesmo de uma função normal, porém, em vez do retorno ser fornecido apenas uma vez, a função geradora entrega os resultados quantas vezes forem necessárias, permitindo que os valores sejam percorridos numa iteração.

Por exemplo, existe uma função no PHP chamada `range()`. Ela cria uma array contendo uma faixa de elementos:

```
php > var_dump(range('a','e'));
array(5) {
  [0]=>
  string(1) "a"
  [1]=>
  string(1) "b"
  [2]=>
  string(1) "c"
  [3]=>
  string(1) "d"
  [4]=>
  string(1) "e"
}
```

Segundo o manual do PHP, se a função `range()` fosse utilizada para criar uma array vazia com 1 milhão de elementos, essa array ocuparia mais de 100MB de memória! Com uma função geradora, o mesmo resultado pode ser obtido com o uso de menos de 1kB.

## 8.1 - Como funciona

Quando uma função geradora é chamada, ela retorna um objeto que pode ser iterado. Sempre que iteramos através desse objeto (com um loop foreach, por exemplo), o PHP chama a função geradora sempre que precisar de um valor e salva o último valor produzido, de modo que a iteração possa ser retomada quando um próximo valor for pedido.

Quando não houver mais valores a serem gerados, a função geradora é encerrada e o fluxo do código continua como se uma array tivesse fornecido os valores da iteração.

## 8.2 - A instrução 'yield'

De modo geral, a instrução `yield` se parece muito com um `return`. Mas, em vez de parar a execução da função e retornar um valor, o `yield` entrega um valor e pausa a execução da função geradora. 

**Exemplo:**

```
php > function genum() { for ($n = 1; $n <= 5; $n++) { yield $n; } }
php > foreach (genum() as $valor) { echo "$valor "; }
1 2 3 4 5 
```

## 8.3 - Gerando valores com índices

Além dos valores simples gerados no exemplo anterior, nós podemos produzir os pares dos índices e valores de uma array associativa (exemplo `gen01.php`):

```
$data = 'id|nome|idade|email
001|João da Silva|23|jsilva@imail.com
002|Maria das Couves|19|mcouves@jmail.com
003|Antônio Pedro|26|apedro@qmail.com';

function cadastro($dt) {

    $linhas = explode("\n", $dt);
    $ncampos = explode("|", trim($linhas[0]));

    array_shift($linhas);

    foreach ($linhas as $linha) {
        $campos = explode("|", $linha);
        yield array_combine($ncampos, $campos);
    }
}

foreach (cadastro($data) as $v) {
    print_r($v);
}
```

O que irá imprimir...

```
:~$ php gen01.php 
Array
(
    [id] => 001
    [nome] => João da Silva
    [idade] => 23
    [email] => jsilva@imail.com
)
Array
(
    [id] => 002
    [nome] => Maria das Couves
    [idade] => 19
    [email] => mcouves@jmail.com
)
Array
(
    [id] => 003
    [nome] => Antônio Pedro
    [idade] => 26
    [email] => apedro@qmail.com
)

```

# 9 - Tratamento de erros

No contexto da programação, um erro é qualquer resultado inseperado na execução de um programa. Se esse resultado inesperado puder ser resolvido alterando o programa, nós dizemos que, de fato, ocorreu um **erro**. Mas, quando a causa do erro está além do próprio programa, ou da nossa capacidade de resolver o problema alterando o código, nós dizemos que houve uma **exceção**.

De forma simplificada, nós podemos dizer:

* **Erros:** problemas relacionados com algo escrito de forma inválida nos nossos códigos.

* **Exceções:** problemas relacionados com algo externo que nós não previmos e que pode levar os nossos programas a falharem de alguma forma.

Sendo assim, o tratamento de erros consiste em aplicar técnicas para lidar com eventuais erros e exceções, prevendo as possibilidades de falha e garantindo que os nossos programas comportem-se da forma esperada. 

## 9.1 - Como o PHP lida com erros

**Ele não lida!**

Quando o assunto são erros e exceções, o máximo que uma linguagem pode fazer é avisar que **nós** fizemos algo errado e oferecer ferramentas para nos ajudar a encontrar uma solução. Então, o verdadeiro título deste tópico deveria ser...

## 9.2 - Como o PHP 'nos ajuda' a lidar com erros

A primeira e mais simples ajuda que o PHP nos oferece está nas mensagens de erro. Através delas, nós temos acesso a pistas que podem nos levar a descobrir onde nós erramos. Essas mensagens estão classificadas em níveis, os quais servem tanto para que o interpretador saiba o que fazer quanto para que nós possamos capturá-las e utilizá-las em alguma técnica de tratamento de erros.

### 9.2.1 - Configurando o relato de erros: 'error_reporting()'

A função `error_reporting()` é utilizada para definir quais erros serão relatados pelo PHP durante a execução do programa. Se não fornecermos o parâmetro opcional NÍVEL, ela retornará um inteiro correspondente ao nível atual de relato de erros:

```
error_reporting([NÍVEL]);
``` 

O nível pode ser passado como um valor inteiro, como uma constante predefinida ou como uma operação bit-a-bit, mas é altamente recomendável o uso das constantes para evitar possíveis diferenças entre versões do PHP.

> **Nota:** a tabela completa das constantes de erro predefinidas e seus respectivos valores pode ser encontrada na documentação do PHP:

https://www.php.net/manual/en/errorfunc.constants.php

Exemplos...

**Desligando todas as notificações:**

```
error_reporting(0);
```

**Reportando todos os erros:**

```
error_reporting(E_ALL);
```

**Ativando apenas algumas notificações**

```
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
```

**Excluindo uma notificação**

```
error_reporting(E_ALL & ~E_NOTICE);
```

> **Importante!** Não sucumba à tentação de esconder a sujeira para debaixo do tapete forçando a ocultação de mensagens de erro. Mesmo no caso de exceções que podem ser causadas pelo usuário ou por uma condição da plataforma de execução, é muito melhor exibir uma mensagem personalizada do que fazer com que o usuário acredite que está tudo bem. 

### 9.2.2 - Personalizando mensagens de erro

O exemplo mais típico de um erro de exceção é a tentativa de abrir um arquivo inexistente:

```
php > $h = fopen('banana.txt', 'r');
PHP Warning:  fopen(banana.txt): failed to open stream: No such file or directory in php shell code on line 1
```

Como dissemos, o erro não deve ser escondido, mas uma mensagem como essa pode confundir o usuário mais do que ajudar. Nesses casos, é altamente recomendável a personalização de uma mensagem de erro, o que pode ser feito facilmente com o construtor de linguagem `exit()`:

```
$arquivo = 'banana.txt';
if (file_exists($arquivo)) {
    $h = fopen($arquivo, 'r');
} else {
    exit("Arquivo $arquivo não encontrado!");
}
```

Mas, como já vimos, o `exit()` (ou seu equivalente, `die()`) faz com que o programa seja encerrado, e essa nem sempre é a melhor alternativa.

> **Importante!** Cada caso é um caso, e não nos cabe aqui determinar o que deve ser feito em situações como a do exemplo. Uma alternativa, porém, seria interagir com o usuário, solicitando outro nome de arquivo ou até deixando para ele a decisão de encerrar o programa com segurança.

### 9.2.3 - Manipuladores de erros personalizados

No PHP, nós podemos criar funções para serem chamadas especificamente na ocorrência de erros. A ideia é basicamente criar uma função que deverá receber pelo menos dois parâmetros obrigatórios: o nível do erro e a mensagem correspondente:

```
function NOME(NÍVEL, MSG[, ARQUIVO, LINHA, CONTEXTO]) { ... }
```

Os parâmetros opcionais dizem respeito a:

* **ARQUIVO:** O arquivo onde ocorreu o erro;
* **LINHA:** A linha em que o erro aconteceu;
* **CONTEXTO:** Uma array com todas as variáveis em uso quando o erro aconteceu e seus valores.

Para que a função seja utilizada, ela precisa ser especificada na função `set_error_handler()`, por exemplo (`cerf.php`):

```
function meus_erros ($num, $msg, $arq, $linha) {
    echo "Nível   : $num\n";
    echo "Mensagem: $msg\n";
    echo "Arquivo : $arq\n";
    echo "Linha   : $linha\n";
    exit;
}

set_error_handler('meus_erros');

echo $a;
```

Aqui, como a variável `$a` não está definida, nós teríamos algo assim na saída:

```
:~$ php cerf.php 
Nível   : 8
Mensagem: Undefined variable: a
Arquivo : /home/blau/gits/curso-phpcli/cerf.php
Linha   : 13
```

Algumas considerações importantes:

* A função personalizada só encerrará o programa se receber uma instrução `exit()` ou `die()`.
* Se não sair, a função será chamada para todas as ocorrências de erro durante a execução do programa.
* Erros de interpretação de sintaxe (parse) não podem ser capturados por funções personalizadas, já que o programa nem chega a ser executado.

### 9.2.4 - Disparando erros com a função 'trigger_error()'

A função `trigger_error()` é utilizada para reagir a uma condição de erro, geralmente uma exceção, e disparar uma resposta específica. Por exemplo (`trig.php`):

```
$valor = readline("Digite um número menor do que 10: ");

if ($valor >= 10) {
    trigger_error("Valor deve ser menor do que 10!");
}
```

Se o usuário digitar um valor maior ou igual a 10, ele verá na saída:

```
PHP Notice:  O número deve ser menor do que 10!
in /home/blau/gits/curso-phpcli/trig.php on line 6
```

> **NOTA:** Por padrão, o erro disparado será do nível `E_USER_NOTICE`, mas a função também aceita os níveis `E_USER_ERROR` e `E_USER_WARNING` se isso for informado como segundo parâmetro opcional.

Em conjunto com funções personalizadas, `trigger_error()` pode nos ajudar a criar mensagens de erro muito mais amigáveis e significativas do que as mensagens padrão do PHP:

```
function meus_erros ($num, $msg, $arq, $linha) {
    echo "Nível   : $num\n";
    echo "Mensagem: $msg\n";
    echo "Arquivo : $arq\n";
    echo "Linha   : $linha\n";
    exit;
}

set_error_handler('meus_erros');

$valor = readline("Digite um número menor do que 10: ");

if ($valor >= 10) {
    trigger_error("O número deve ser menor do que 10!");
}
```

Neste caso, a saída de erro seria...

```
Nível   : 1024
Mensagem: O número deve ser menor do que 10!
Arquivo : /home/blau/gits/curso-phpcli/trig.php
Linha   : 16
```

## 9.3 - Prevendo erros e exceções

A melhor forma de lidar com erros e exceções é antecipando o que pode dar errado. Para isso, nós podemos contar com as diversas funções do PHP capazes de testar uma infinidade de condições, por exemplo:

| Função          | Descrição                            |
|-----------------|--------------------------------------|
| `file_exists()` | testa se um arquivo existe.          |
| `is_writable()` | testa se um arquivo permite escrita. |
| `isset()`       | testa se uma variável está definida. |
| `defined()`     | testa se uma constante foi definida. |
| `is_array()`    | testa se a expressão é uma array.    |
| `is_resource()` | testa se a expressão é um recurso.   |

Como essas, existem muitas outras, e é bastante recomendável o seu uso para validar condições críticas.

## 9.4 - Tratando erros com a classe 'Exception'

O PHP oferece uma forma bem mais precisa e completa para o tratamento de erros: a classe `Exception`. Embora o assunto "orientação a objetos" fuja um pouco dos nossos objetivos, é importante que você saiba que este recurso existe para poder se aprofundar quando tiver oportunidade.

Por enquanto, tudo que precisamos saber é que uma classe é um conceito da orientação a objetos e que ela pode conter várias funções -- neste caso, chamadas de métodos. O poder da classe `Exception` vem justamente de seus vários métodos, os quais nos permitem capturar e manipular detalhadamente os erros.

A classe `Exception` geralemente é utilizada numa estrutura desse tipo:

```
try {

    // INSTRUÇÕES...

    if (CONDIÇÃO) {
        throw new Exception('MENSAGEM'[, CÓDIGO]);
    }
} 
catch (Exception $NOME) {

    //INSTRUÇÕES...
    
    echo $NOME->MÉTODO();
    
    // INSTRUÇÕES...
}
```

Onde...

* `try` é o bloco onde o código tem o potencial de falhar;
* `CONDIÇÃO` é a condição que será considerada um erro;
* `throw new` é a instrução que "lança" uma nova instância de `Exception`, define uma MENSAGEM e, opcionalmente, um CÓDIGO numérico para o erro;
* `catch` é o bloco que captura as instâncias de `Exception` que forem lançadas;
* `$NOME` é o nome que vai identificar a instância de `Exception` que for capturada;
* `$NOME->MÉTODO()` é como nós chamamos um método da classe `Exception`.

Para cada rotina com potencial de falha (abrir um arquivo, uma conexão com um banco de dados, etc), nós criamos um bloco `try` e todos eles serão monitorados por um único bloco `catch`. Cada um dos  blocos `try` irá lançar a sua própria instância da classe `Exception` quando (ou se) houver uma falha detectada em CONDIÇÃO, e isso fará com que o bloco `catch` seja executado.

Por exemplo (`try.php`):

```
try {
    $f = 'banana.txt';

    // Se o arquivo não existir, uma nova 'Exception' é lançada...
    if ( !file_exists($f) ) {
        throw new Exception("Arquivo $f não existe!", 30);
    }
}

// O bloco 'catch' captura o erro e exibe as informações...
catch (Exception $erro) {
    echo $erro->getMessage()."\n";
    echo $erro->getCode()."\n";
    echo $erro->getFile()."\n";
    echo $erro->getLine()."\n";
}
```

Como resultado, nós teríamos na saída...

```
:~$ php try.php
Arquivo banana.txt não existe!
30
/home/blau/gits/curso-phpcli/try.php
7
```
