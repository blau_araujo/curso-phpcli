# PHP-CLI - Aula 7: Funções

Uma função é um conjunto de instruções agrupadas de forma a poderem ser invocadas de qualquer parte do código para executarem um procedimento e/ou retornarem algum valor. Até certo ponto, nós podemos entender as funções no PHP como "programas dentro dos nossos programas" (daí também serem chamadas de "sub-rotinas"):

* Elas geralmente têm um nome pelo qual podem ser chamadas;
* Podem aceitar parâmetros de execução;
* Podem retornar valores;
* Podem exibir saídas;
* Podem capturar entradas;
* Podem manipular arquivos...

Enfim, tal como os nossos programas em PHP.

A única diferença é que as funções são pensadas para executarem procedimentos  muito mais específicos (daí também serem chamadas de "métodos") que precisaríamos executar várias vezes nos nossos códigos.

Como já vimos nos primeiros tópicos, o PHP fornece uma infinidade de funções prontas para serem utilizadas nos nossos programas, as funções *built-in*, mas também nos dá a possibilidade de criarmos as nossas próprias, e é disso que falaremos neste tópico.

## 7.1 - Por que criar funções

Os motivos são muito bons:

* **Organização:** com as funções, nós podemos separar cada trecho do nosso código conforme a tarefa que executa, o que torna a manutenção muito mais fácil.

* **Reutilização:** basta escrevermos uma tarefa repetitiva uma única vez para chamá-la quantas vezes quisermos.

* **Modularidade:** conjuntos de funções permitem que os nossos códigos sejam vistos como a soma de vários pequenos módulos especializados em uma tarefa, como blocos de um Lego que podemos montar conforme a necessidade.

* **Bibliotecas pessoais:** com o tempo, muitas das nossas funções podem acabar solucionando problemas que encontraremos em vários projetos, o que permite até a criação de bibliotecas pessoais ou pequenos *frameworks* com o código reaproveitado.

## 7.2 - Anatomia de uma função

No PHP, as funções são criadas com a palavra-chave `function` seguida de um nome, uma lista de parâmetros de execução entre parêntesis e um agrupamento de instruções entre chaves:

```
function NOME([par_1, par_2, ... , par_n]) {
    INSTRUÇÕES...
    return VALOR;
}
```

### Nome da função

O nome da função segue as mesmas regras de outros identificadores do PHP: deve começar com uma letra ou com o caractere sublinhado seguido de qualquer quantidade de letras, números e sublinhados.

### Lista de parâmetros

As funções no PHP podem receber zero ou mais parâmetros, que são uma forma de passarmos os dados que queremos elas processem. A lista de parâmetros consiste de zero ou mais variáveis separadas por vírgulas, com ou sem valores atribuídos por padrão.

Exemplo:

```
function soma($a, $b) {
    $c = $a + $b;
    return $c;
}
```

Quando chamada, a função `soma()` vai exigir que os valores de `$a` e `$b` sejam informados, o que deve ser feito respeitando o tipo de dado exigido e a ordem em que os parâmetros aparecem na declaração da função. Se houver mais de um, como no exemplo, eles devem ser separados por vírgula:

```
soma(4, 3);
     ^  ^
     |  |
    $a $b
```

Como vimos no tópico "Escopo de variáveis", todas as variáveis definidas dentro das funções, inclusive aquelas que serão utilizadas como parâmetros, têm escopo restrito apenas ao contexto da função, ou seja, não são visíveis de outras partes do código.

> **Nota:** lembre-se de que o contrário também é verdadeiro -- as variáveis do escopo global também não são vistas (por padrão) dentro de funções.

Se a função não receber parâmetros, ela deve ser chamada com os parêntesis vazios:

```
minha_funcao();
```

### Grupo de instruções

As chaves delimitam o conjunto de instruções que serão executadas pelas funções, e elas podem ser qualquer código válido em PHP, inclusive outras funções que nós criarmos.

### Instrução 'return'

As funções podem "devolver" os dados que ela processar de várias formas: alterando uma ou mais variáveis globais, exibindo algo na saída, encerrando a execução do programa com um código de erro, etc. Mas a forma padrão de retornar valores é através da instrução opcional `return`.

```
return [EXPRESSÃO];
```

O `return` faz com que a função termine a sua execução imediatamente, devolvendo o controle ao código na linha em que ela tiver sido chamada. Seu argumento opcional é o valor de uma EXPRESSÃO, e é isso que será retornado para o código. Se a instrução `return` for utilizada sem argumentos, o valor retornado pela função será `NULL`. 

> **Nota:** a instrução `return` também pode ser chamada do corpo principal do código, causando o fim da execução do script.

## 7.3 - Como funcionam as funções

É muito importante entender que uma função, quando chamada, faz com que o script seja pausado até que ela devolva o controle ao fluxo principal do código no ponto em que a invocação aconteceu, retornando o valor de uma expressão ou `NULL`:

**Função com retorno nulo:**

```
php > function teste() { echo "Isso é um teste!\n"; }
php > var_dump(teste());
Isso é um teste!
NULL
```

**Função com o retorno do valor de uma expressão**

```
php > function soma($a, $b) { return $a + $b; }
php > var_dump(soma(2,3));
int(5)
```

Outro ponto importante, é que as funções podem ser chamadas de qualquer ponto no código, inclusive de uma linha anterior à da sua declaração:

**Exemplo (func01.php):**

```
echo soma(2,3)."\n";

function soma($a, $b) {
    return $a + $b;
}
```

Executando...

```
:~$ php func01.php 
5
```

Contudo, nós podemos condicionar a declaração de uma função para que ela só esteja disponível caso alguma condição seja atendida:

**Exemplo (func02.php):**

```
if (isset($argv[1])) {
    function soma($a, $b) {
        return $a + $b;
    }
}

if (function_exists("soma")) {
    echo soma(3,4)."\n";
} else {
    echo "Função não existe!\n";
}
```

Executando...

```
:~$ php func02.php
Função não existe!

:~$ func02.php teste
7
```

Neste caso, a chamada da função só pode ocorrer como no exemplo acima, **depois do ponto em que a função é declarada**. O exemplo abaixo faria `function_exists()` retornar sempre `false`, mesmo que um argumento fosse passado para o script:


```
if (function_exists("soma")) {
    echo soma(3,4)."\n";
} else {
    echo "Função não existe!\n";
}

if (isset($argv[1])) {
    function soma($a, $b) {
        return $a + $b;
    }
}
```

Outra forma de condicionar a disponibilidade de uma função é criá-la dentro de outra função.

**Exemplo (func03.php):**

```
if (!function_exists('existo')) {
    echo "Se não pensar, eu não existo!\n"
}

penso();

function penso() {

    echo "Penso...\n";

    function existo() {
        echo "logo existo!\n";
    }
}

existo();
```

## 7.4 - Funções variáveis

O PHP tem o conceito de **funções variáveis**, ou seja, se uma variável aparecer seguida de parêntesis, o interpretador irá procurar por uma função cujo nome seja o valor da variável e a executará.

**Exemplo (func04.php):**

```
function penso() {
    global $func2;

    echo "Penso, ";
    $func2();
}

function existo() {
    echo "logo existo.\n";
}

$func1 = 'penso';
$func2 = 'existo';

$func1();
```

## 7.5 - Funções anônimas

O PHP também permite a criação de funções anônimas, também chamadas de *closures*. Embora apareçam mais como parâmetros de callback de outras funções, elas têm muitos outros usos interessantes. Por exemplo, nós podemos atribuir funções a variáveis:

```
php > $salve = function($nome) { echo "Olá, $nome!\n"; };
php > $salve('Blau');
Olá, Blau!
```

> **Importante!** A atribuição de funções a variáveis deve terminar com `;`!

## 7.6 - Parâmetros obrigatórios e opcionais

Como dissemos, os parâmetros de uma função podem ser obrigatórios ou opcionais. Por padrão, isso depende de como nós definimos as variáveis na lista de parâmetros: 

```
function NOME($OBRIGATÓRIO, $OPCIONAL=VALOR_PADRÃO) { ... }
```

Se a variável for iniciada com um valor padrão, o parâmetro será opcional.

Isso é muito útil, mas pode trazer alguns problemas quando tivermos mais de um parâmetro opcional:

```
php > function soma($a = 5, $b = 4) { return $a + $b; }
```

Neste exemplo, nós só temos a opção de não passarmos nenhum parâmetro ou não passarmos o segundo (`$b = 4`):

```
php > echo soma();
9
php > echo soma(10);
14
php > echo soma(6,9);
15
```

Isso acontece porque os parâmetros das funções são sempre posicionais, ou seja, eles estão amarrados à ordem em que aparecem na declaração da função, o que acaba limitando a utilidade do recurso a quando temos apenas um parâmetro opcional (geralmente definido por último na lista).

## 7.7 - Passagem de argumentos por valor e por referência

Por padrão, os argumentos são passados por valor, o que faz com que os valores dos argumentos sejam alterados apenas dentro das funções. Para alterar argumentos fora das funções, nós podemos passá-los por referência, o que é feito incluindo o prefixo `&` ao nome da variável na lista de parâmetros que será recebida por referência. Por exemplo:

```
php > function fruta(&$str) { $str .= ' laranja'; }
php > $a = 'banana';
php > fruta($a);
php > echo $a;
banana laranja
```

## 7.8 - Número variável de parâmetros

A partir do PHP 5.6, é possível criar funções que aceitam um número variável de parâmetros prefixando a variável que receberá os argumentos com `...`, por exemplo:

```
php > function soma(...$valores) { return array_sum($valores); }
php > echo soma(1,2,3);
6
```

Isso é o equivalente (menos trabalhoso) de fazer:

```
php > function soma($valores=[]) { return array_sum($valores); }
php > echo soma([1,2,3]);
6
```

Ou então...

```
php > function soma() { return array_sum(func_get_args()); }
php > echo soma(1,2,3);
6
```

