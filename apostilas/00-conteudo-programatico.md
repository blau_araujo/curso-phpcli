# Curso: PHP-CLI

## [1 - Conceitos básicos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/01-conceitos-basicos.md)

1. [Módulos essenciais](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/01-conceitos-basicos.md#11-instala%C3%A7%C3%A3o-e-m%C3%B3dulos-essenciais)
2. [Opções da linha de comando](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/01-conceitos-basicos.md#12-op%C3%A7%C3%B5es-da-linha-de-comando)
3. [O modo interativo](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/01-conceitos-basicos.md#13-o-modo-interativo)
4. [O servidor interno](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/01-conceitos-basicos.md#14-o-servidor-embutido)
5. [Como executar os scripts](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/01-conceitos-basicos.md#15-como-executar-os-scripts)

## [2 - Interagindo com o Shell](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/02-interagindo-com-o-shell.md)

1. [Instruções, funções e construtores de linguagem](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/02-interagindo-com-o-shell.md#21-instru%C3%A7%C3%B5es-fun%C3%A7%C3%B5es-e-construtores-de-linguagem)
2. [Exibindo saídas no terminal](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/02-interagindo-com-o-shell.md#22-exibindo-sa%C3%ADdas-no-terminal)
3. [Parâmetros posicionais](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/02-interagindo-com-o-shell.md#23-par%C3%A2metros-posicionais)
4. [Lendo a entrada padrão](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/02-interagindo-com-o-shell.md#24-lendo-a-entrada-padr%C3%A3o)
5. [Executando comandos externos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/02-interagindo-com-o-shell.md#25-executando-comandos-externos)

## [3 - Variáveis e tipos de dados](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md)

1. [Nomeando variáveis](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#31-nomeando-vari%C3%A1veis)
2. [Expressões](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#32-express%C3%B5es)
3. [Atribuição por valor e por referência](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#33-atribui%C3%A7%C3%A3o-por-valor-e-por-refer%C3%AAncia)
4. [Variáveis não inicializadas](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#34-vari%C3%A1veis-n%C3%A3o-inicializadas)
5. [Escopo de variáveis](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#35-escopo-das-vari%C3%A1veis)
6. [Variáveis variáveis](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#36-vari%C3%A1veis-vari%C3%A1veis)
7. [Tipos de dados](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#37-tipos-de-dados)
8. [Arrays](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#38-arrays)
9. [Constantes](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/03-variaveis-e-tipos.md#39-constantes)

## [4 - Trabalhando com arquivos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md)

1. [Listar diretórios](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#41-listar-diret%C3%B3rios)
2. [Buscar arquivos e pastas segundo um padrão](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#42-buscar-arquivos-e-pastas-segundo-um-padr%C3%A3o)
3. [Comparar nomes de arquivos com um padrão](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#43-comparar-nomes-de-arquivos-com-um-padr%C3%A3o)
4. [Testar atributos de arquivos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#44-testar-atributos-de-arquivos)
5. [Obtendo informações de caminhos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#45-obtendo-informa%C3%A7%C3%B5es-de-caminhos)
6. [Operações comuns com arquivos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#46-opera%C3%A7%C3%B5es-comuns-com-arquivos)
7. [Leitura e escrita de arquivos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#47-leitura-e-escrita-de-arquivos)
8. [Streams de entrada e saída](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/04-trabalhando-com-arquivos-e-pastas.md#48-streams-de-entrada-e-sa%C3%ADda)

## [5 - Operadores](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md)

1. [Precedência e agrupamento de operadores](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#51-precec%C3%AAncia-e-agrupamento-de-operadores)
2. [Operadores aritméticos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#52-operadores-aritm%C3%A9ticos)
3. [Operadores de atribuição](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#53-operadores-de-atribui%C3%A7%C3%A3o)
4. [Operadores bit-a-bit (*bitwise*)](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#54-operadores-bit-a-bit-bitwise)
5. [Operadores de comparação](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#55-operadores-de-compara%C3%A7%C3%A3o)
6. [Operadores lógicos](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#56-operadores-l%C3%B3gicos)
7. [Operador de execução](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/05-operadores.md#57-operador-de-execu%C3%A7%C3%A3o)

## [6 - Estruturas de controle de fluxo](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md)

1. [As cláusulas 'if/elseif/esle'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#61-as-cl%C3%A1usulas-ifelseifelse)
2. [A estrutura 'switch/case'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#62-a-estrutura-switchcase)
3. [O operador 'goto'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#63-o-operador-goto)
4. [O loop 'for'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#64-o-loop-for)
5. [O loop 'while'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#65-o-loop-while)
6. [O loop 'do/while'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#66-o-loop-dowhile)
7. [Interrompendo loops](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#67-interrompendo-loops)
8. [Saltando o restante da iteração](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/06-estruturas-de-controle-de-fluxo.md#68-saltando-o-restante-da-itera%C3%A7%C3%A3o)

## [7 - Funções](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md)

1. [Por que criar funções](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#71-por-que-criar-fun%C3%A7%C3%B5es)
2. [Anatomia de uma função](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#72-anatomia-de-uma-fun%C3%A7%C3%A3o)
3. [Como funcionam as funções](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#73-como-funcionam-as-fun%C3%A7%C3%B5es)
4. [Funções variáveis](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#74-fun%C3%A7%C3%B5es-vari%C3%A1veis)
5. [Funções anônimas](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#75-fun%C3%A7%C3%B5es-an%C3%B4nimas)
6. [Parâmetros obrigatórios e opcionais](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#76-par%C3%A2metros-obrigat%C3%B3rios-e-opcionais)
7. [Passagem de argumentos por valor e por referência](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#77-passagem-de-argumentos-por-valor-e-por-refer%C3%AAncia)
8. [Número variável de parâmetros](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/07-funcoes.md#78-n%C3%BAmero-vari%C3%A1vel-de-par%C3%A2metros)

## [8 - Funções geradoras](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/08-geradores.md)

1. [Como funciona](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/08-geradores.md#81-como-funciona)
2. [A instrução 'yeld'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/08-geradores.md#82-a-instru%C3%A7%C3%A3o-yield)
3. [Gerando valores com índices](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/08-geradores.md#83-gerando-valores-com-%C3%ADndices)

## [9 - Tratamento de erros](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/09-tratamento-de-erros.md)

1. [Como o PHP lida com erros](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/09-tratamento-de-erros.md#91-como-o-php-lida-com-erros)
2. [Como o PHP 'nos ajuda' a lidar com erros](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/09-tratamento-de-erros.md#92-como-o-php-nos-ajuda-a-lidar-com-erros)
3. [Prevendo erros e exceções](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/09-tratamento-de-erros.md#93-prevendo-erros-e-exce%C3%A7%C3%B5es)
4. [Tratando erros com a classe 'Exception'](https://gitlab.com/blau_araujo/curso-phpcli/-/blob/master/apostilas/09-tratamento-de-erros.md#94-tratando-erros-com-a-classe-exception)

