# PHP-CLI - Aula 8: Funções geradoras

Uma função geradora permite a criação de um conjunto de dados sem a necessidade de carregar uma array, o que pode, em alguns casos, sobrecarregar a memória e os recursos de processamento disponíveis.

O comportamento é o mesmo de uma função normal, porém, em vez do retorno ser fornecido apenas uma vez, a função geradora entrega os resultados quantas vezes forem necessárias, permitindo que os valores sejam percorridos numa iteração.

Por exemplo, existe uma função no PHP chamada `range()`. Ela cria uma array contendo uma faixa de elementos:

```
php > var_dump(range('a','e'));
array(5) {
  [0]=>
  string(1) "a"
  [1]=>
  string(1) "b"
  [2]=>
  string(1) "c"
  [3]=>
  string(1) "d"
  [4]=>
  string(1) "e"
}
```

Segundo o manual do PHP, se a função `range()` fosse utilizada para criar uma array vazia com 1 milhão de elementos, essa array ocuparia mais de 100MB de memória! Com uma função geradora, o mesmo resultado pode ser obtido com o uso de menos de 1kB.

## 8.1 - Como funciona

Quando uma função geradora é chamada, ela retorna um objeto que pode ser iterado. Sempre que iteramos através desse objeto (com um loop foreach, por exemplo), o PHP chama a função geradora sempre que precisar de um valor e salva o último valor produzido, de modo que a iteração possa ser retomada quando um próximo valor for pedido.

Quando não houver mais valores a serem gerados, a função geradora é encerrada e o fluxo do código continua como se uma array tivesse fornecido os valores da iteração.

## 8.2 - A instrução 'yield'

De modo geral, a instrução `yield` se parece muito com um `return`. Mas, em vez de parar a execução da função e retornar um valor, o `yield` entrega um valor e pausa a execução da função geradora. 

**Exemplo:**

```
php > function genum() { for ($n = 1; $n <= 5; $n++) { yield $n; } }
php > foreach (genum() as $valor) { echo "$valor "; }
1 2 3 4 5 
```

## 8.3 - Gerando valores com índices

Além dos valores simples gerados no exemplo anterior, nós podemos produzir os pares dos índices e valores de uma array associativa (exemplo `gen01.php`):

```
$data = 'id|nome|idade|email
001|João da Silva|23|jsilva@imail.com
002|Maria das Couves|19|mcouves@jmail.com
003|Antônio Pedro|26|apedro@qmail.com';

function cadastro($dt) {

    $linhas = explode("\n", $dt);
    $ncampos = explode("|", trim($linhas[0]));

    array_shift($linhas);

    foreach ($linhas as $linha) {
        $campos = explode("|", $linha);
        yield array_combine($ncampos, $campos);
    }
}

foreach (cadastro($data) as $v) {
    print_r($v);
}
```

O que irá imprimir...

```
:~$ php gen01.php 
Array
(
    [id] => 001
    [nome] => João da Silva
    [idade] => 23
    [email] => jsilva@imail.com
)
Array
(
    [id] => 002
    [nome] => Maria das Couves
    [idade] => 19
    [email] => mcouves@jmail.com
)
Array
(
    [id] => 003
    [nome] => Antônio Pedro
    [idade] => 26
    [email] => apedro@qmail.com
)

```

