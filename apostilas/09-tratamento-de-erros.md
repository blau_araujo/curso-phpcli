# PHP-CLI - Aula 9: Tratamento de erros

No contexto da programação, um erro é qualquer resultado inseperado na execução de um programa. Se esse resultado inesperado puder ser resolvido alterando o programa, nós dizemos que, de fato, ocorreu um **erro**. Mas, quando a causa do erro está além do próprio programa, ou da nossa capacidade de resolver o problema alterando o código, nós dizemos que houve uma **exceção**.

De forma simplificada, nós podemos dizer:

* **Erros:** problemas relacionados com algo escrito de forma inválida nos nossos códigos.

* **Exceções:** problemas relacionados com algo externo que nós não previmos e que pode levar os nossos programas a falharem de alguma forma.

Sendo assim, o tratamento de erros consiste em aplicar técnicas para lidar com eventuais erros e exceções, prevendo as possibilidades de falha e garantindo que os nossos programas comportem-se da forma esperada. 

## 9.1 - Como o PHP lida com erros

**Ele não lida!**

Quando o assunto são erros e exceções, o máximo que uma linguagem pode fazer é avisar que **nós** fizemos algo errado e oferecer ferramentas para nos ajudar a encontrar uma solução. Então, o verdadeiro título deste tópico deveria ser...

## 9.2 - Como o PHP 'nos ajuda' a lidar com erros

A primeira e mais simples ajuda que o PHP nos oferece está nas mensagens de erro. Através delas, nós temos acesso a pistas que podem nos levar a descobrir onde nós erramos. Essas mensagens estão classificadas em níveis, os quais servem tanto para que o interpretador saiba o que fazer quanto para que nós possamos capturá-las e utilizá-las em alguma técnica de tratamento de erros.

### 9.2.1 - Configurando o relato de erros: 'error_reporting()'

A função `error_reporting()` é utilizada para definir quais erros serão relatados pelo PHP durante a execução do programa. Se não fornecermos o parâmetro opcional NÍVEL, ela retornará um inteiro correspondente ao nível atual de relato de erros:

```
error_reporting([NÍVEL]);
``` 

O nível pode ser passado como um valor inteiro, como uma constante predefinida ou como uma operação bit-a-bit, mas é altamente recomendável o uso das constantes para evitar possíveis diferenças entre versões do PHP.

> **Nota:** a tabela completa das constantes de erro predefinidas e seus respectivos valores pode ser encontrada na documentação do PHP:

https://www.php.net/manual/en/errorfunc.constants.php

Exemplos...

**Desligando todas as notificações:**

```
error_reporting(0);
```

**Reportando todos os erros:**

```
error_reporting(E_ALL);
```

**Ativando apenas algumas notificações**

```
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
```

**Excluindo uma notificação**

```
error_reporting(E_ALL & ~E_NOTICE);
```

> **Importante!** Não sucumba à tentação de esconder a sujeira para debaixo do tapete forçando a ocultação de mensagens de erro. Mesmo no caso de exceções que podem ser causadas pelo usuário ou por uma condição da plataforma de execução, é muito melhor exibir uma mensagem personalizada do que fazer com que o usuário acredite que está tudo bem. 

### 9.2.2 - Personalizando mensagens de erro

O exemplo mais típico de um erro de exceção é a tentativa de abrir um arquivo inexistente:

```
php > $h = fopen('banana.txt', 'r');
PHP Warning:  fopen(banana.txt): failed to open stream: No such file or directory in php shell code on line 1
```

Como dissemos, o erro não deve ser escondido, mas uma mensagem como essa pode confundir o usuário mais do que ajudar. Nesses casos, é altamente recomendável a personalização de uma mensagem de erro, o que pode ser feito facilmente com o construtor de linguagem `exit()`:

```
$arquivo = 'banana.txt';
if (file_exists($arquivo)) {
    $h = fopen($arquivo, 'r');
} else {
    exit("Arquivo $arquivo não encontrado!");
}
```

Mas, como já vimos, o `exit()` (ou seu equivalente, `die()`) faz com que o programa seja encerrado, e essa nem sempre é a melhor alternativa.

> **Importante!** Cada caso é um caso, e não nos cabe aqui determinar o que deve ser feito em situações como a do exemplo. Uma alternativa, porém, seria interagir com o usuário, solicitando outro nome de arquivo ou até deixando para ele a decisão de encerrar o programa com segurança.

### 9.2.3 - Manipuladores de erros personalizados

No PHP, nós podemos criar funções para serem chamadas especificamente na ocorrência de erros. A ideia é basicamente criar uma função que deverá receber pelo menos dois parâmetros obrigatórios: o nível do erro e a mensagem correspondente:

```
function NOME(NÍVEL, MSG[, ARQUIVO, LINHA, CONTEXTO]) { ... }
```

Os parâmetros opcionais dizem respeito a:

* **ARQUIVO:** O arquivo onde ocorreu o erro;
* **LINHA:** A linha em que o erro aconteceu;
* **CONTEXTO:** Uma array com todas as variáveis em uso quando o erro aconteceu e seus valores.

Para que a função seja utilizada, ela precisa ser especificada na função `set_error_handler()`, por exemplo (`cerf.php`):

```
function meus_erros ($num, $msg, $arq, $linha) {
    echo "Nível   : $num\n";
    echo "Mensagem: $msg\n";
    echo "Arquivo : $arq\n";
    echo "Linha   : $linha\n";
    exit;
}

set_error_handler('meus_erros');

echo $a;
```

Aqui, como a variável `$a` não está definida, nós teríamos algo assim na saída:

```
:~$ php cerf.php 
Nível   : 8
Mensagem: Undefined variable: a
Arquivo : /home/blau/gits/curso-phpcli/cerf.php
Linha   : 13
```

Algumas considerações importantes:

* A função personalizada só encerrará o programa se receber uma instrução `exit()` ou `die()`.
* Se não sair, a função será chamada para todas as ocorrências de erro durante a execução do programa.
* Erros de interpretação de sintaxe (parse) não podem ser capturados por funções personalizadas, já que o programa nem chega a ser executado.

### 9.2.4 - Disparando erros com a função 'trigger_error()'

A função `trigger_error()` é utilizada para reagir a uma condição de erro, geralmente uma exceção, e disparar uma resposta específica. Por exemplo (`trig.php`):

```
$valor = readline("Digite um número menor do que 10: ");

if ($valor >= 10) {
    trigger_error("Valor deve ser menor do que 10!");
}
```

Se o usuário digitar um valor maior ou igual a 10, ele verá na saída:

```
PHP Notice:  O número deve ser menor do que 10!
in /home/blau/gits/curso-phpcli/trig.php on line 6
```

> **NOTA:** Por padrão, o erro disparado será do nível `E_USER_NOTICE`, mas a função também aceita os níveis `E_USER_ERROR` e `E_USER_WARNING` se isso for informado como segundo parâmetro opcional.

Em conjunto com funções personalizadas, `trigger_error()` pode nos ajudar a criar mensagens de erro muito mais amigáveis e significativas do que as mensagens padrão do PHP:

```
function meus_erros ($num, $msg, $arq, $linha) {
    echo "Nível   : $num\n";
    echo "Mensagem: $msg\n";
    echo "Arquivo : $arq\n";
    echo "Linha   : $linha\n";
    exit;
}

set_error_handler('meus_erros');

$valor = readline("Digite um número menor do que 10: ");

if ($valor >= 10) {
    trigger_error("O número deve ser menor do que 10!");
}
```

Neste caso, a saída de erro seria...

```
Nível   : 1024
Mensagem: O número deve ser menor do que 10!
Arquivo : /home/blau/gits/curso-phpcli/trig.php
Linha   : 16
```

## 9.3 - Prevendo erros e exceções

A melhor forma de lidar com erros e exceções é antecipando o que pode dar errado. Para isso, nós podemos contar com as diversas funções do PHP capazes de testar uma infinidade de condições, por exemplo:

| Função          | Descrição                            |
|-----------------|--------------------------------------|
| `file_exists()` | testa se um arquivo existe.          |
| `is_writable()` | testa se um arquivo permite escrita. |
| `isset()`       | testa se uma variável está definida. |
| `defined()`     | testa se uma constante foi definida. |
| `is_array()`    | testa se a expressão é uma array.    |
| `is_resource()` | testa se a expressão é um recurso.   |

Como essas, existem muitas outras, e é bastante recomendável o seu uso para validar condições críticas.

## 9.4 - Tratando erros com a classe 'Exception'

O PHP oferece uma forma bem mais precisa e completa para o tratamento de erros: a classe `Exception`. Embora o assunto "orientação a objetos" fuja um pouco dos nossos objetivos, é importante que você saiba que este recurso existe para poder se aprofundar quando tiver oportunidade.

Por enquanto, tudo que precisamos saber é que uma classe é um conceito da orientação a objetos e que ela pode conter várias funções -- neste caso, chamadas de métodos. O poder da classe `Exception` vem justamente de seus vários métodos, os quais nos permitem capturar e manipular detalhadamente os erros.

A classe `Exception` geralemente é utilizada numa estrutura desse tipo:

```
try {

    // INSTRUÇÕES...

    if (CONDIÇÃO) {
        throw new Exception('MENSAGEM'[, CÓDIGO]);
    }
} 
catch (Exception $NOME) {

    //INSTRUÇÕES...
    
    echo $NOME->MÉTODO();
    
    // INSTRUÇÕES...
}
```

Onde...

* `try` é o bloco onde o código tem o potencial de falhar;
* `CONDIÇÃO` é a condição que será considerada um erro;
* `throw new` é a instrução que "lança" uma nova instância de `Exception`, define uma MENSAGEM e, opcionalmente, um CÓDIGO numérico para o erro;
* `catch` é o bloco que captura as instâncias de `Exception` que forem lançadas;
* `$NOME` é o nome que vai identificar a instância de `Exception` que for capturada;
* `$NOME->MÉTODO()` é como nós chamamos um método da classe `Exception`.

Para cada rotina com potencial de falha (abrir um arquivo, uma conexão com um banco de dados, etc), nós criamos um bloco `try` e todos eles serão monitorados por um único bloco `catch`. Cada um dos  blocos `try` irá lançar a sua própria instância da classe `Exception` quando (ou se) houver uma falha detectada em CONDIÇÃO, e isso fará com que o bloco `catch` seja executado.

Por exemplo (`try.php`):

```
try {
    $f = 'banana.txt';

    // Se o arquivo não existir, uma nova 'Exception' é lançada...
    if ( !file_exists($f) ) {
        throw new Exception("Arquivo $f não existe!", 30);
    }
}

// O bloco 'catch' captura o erro e exibe as informações...
catch (Exception $erro) {
    echo $erro->getMessage()."\n";
    echo $erro->getCode()."\n";
    echo $erro->getFile()."\n";
    echo $erro->getLine()."\n";
}
```

Como resultado, nós teríamos na saída...

```
:~$ php try.php
Arquivo banana.txt não existe!
30
/home/blau/gits/curso-phpcli/try.php
7
```

