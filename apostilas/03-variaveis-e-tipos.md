# 3 - Variáveis e tipos de dados

Uma variável é uma região na memória onde uma certa informação é armazenada temporariamente. No PHP, esses locais são identificados por nomes precedidos pelo cifrão (`$`) e a forma de atribuição mais básica é feita com o operador de atribuição `=`.

## 3.1 - Nomeando variáveis

Os nomes de variáveis devem começar com letras maiúsculas, minúsculas ou o caractere sublinhado seguidos por qualquer quantidade de letras, números e sublinhados. O PHP aceita nomes de variáveis contendo caracteres da tabela ASCII estendida (bytes de 128 a 255), mas utilizar acentos, cedilha e outros símbolos gráficos, embora funcionem, geralmente não é considerado uma boa prática.


**Exemplo 1:**

```
php > $var = 'banana';
php > $Var = 'laranja';
```

Como os nomes de variáveis são sensíveis à caixa, `$var` e `$Var` identificam duas variáveis diferentes:

```
php > echo "$var $Var";
banana laranja
```

**Exemplo 2:**

```
php > $_nome = 'Maria';   // Nome válido
php > $ação = 'copiar';   // Nome válido
php > $_2var = 'teste';   // Válido, começa com '_'
```

**São proibidos:**

* Números no início do nome após o cifrão
* O nome da variável especial `$this`

**Exemplo 3:**

```
php > $2url = 'teste.org' // Inválido, começa com número
```

> **Nota:** Para eliminar variáveis, nós podemos utilizar a função `unset()`:

```
php > $a = 'banana';
php > echo $a;
banana
php > unset($a);
php > echo $a;
PHP Notice:  Undefined variable: a in php shell code on line 1
```

## 3.2 - Expressões

As expressões são os elementos mais importantes do PHP, e quase tudo que escrevemos no nosso código são expressões. De forma bem objetiva, o manual do PHP define expressões como: "qualquer coisa que tenha um valor", ou seja, através das expressões que escrevemos, nós representamos valores.

As formas mais básicas de expressões são as variáveis e as constantes. Quando escrevemos `$a = 10`, nós estamos atribuindo o valor `10` ao nome `$a`, onde `10` é a expressão que representa o valor `10`. Após a atribuição, `$a` também terá o valor `10` e, se escrevermos `$b = $a`, nesta atribuição, `$a` será uma expressão que representa o valor `10`.

Indo um pouco mais fundo, a própria atribuição `$a = 10` é uma expressão e também representa um valor (10, neste exemplo). Isso significa que podemos fazer algo como:

```
php > $c = $a = 10;
php > echo $c;
10
```

Nós falaremos mais sobre isso adiante, mas é óbvio que nem todos os valores serão números inteiros. O PHP suporta 4 tipos de valores escalares, ou seja, valores que não podem ser decompostos em outros valores: 

* Números inteiros (integer)
* Números de ponto flutuante (float)
* Strings (cadeias de caracteres)
* Valores booleanos

Além disso, há dois tipos de valores não-escalares (ou compostos):

* Vetores (arrays)
* Objetos 

Todos esses tipos de valores podem ser atribuídos a uma variável ou retornar de uma função.


## 3.3 - Atribuição por valor e por referência

No PHP, as variáveis podem ser atribuídas **por valor** ou **por referência**.

**Atribuição por valor**

Quando uma expressão é atribuída a uma variável, todo o valor original da expressão é copiado para a variável. Neste caso, nenhuma atribuição subsequente desta variável a outras variáveis, que porventura venham a ser alteradas posteriormente, afetarão o valor da variável original:

```
php > $a = 10;
php > $b = $a;
php > echo "$a, $b";
10, 10
php > $b = $a + 1;
php > echo "$a, $b";
10, 11
```

> **Esta é a forma padrão de atribuição de variáveis no PHP.**

**Atribuição por referência**

É quando uma variável recebe a referência a outra variável em vez do seu valor, como se estivéssemos criando um apelido (alias) ou apontando para a variável original. Portanto, mudanças nesta variável também afetarão a variável original:

```
php > $a = 10;
php > $b = &$a;
php > $b++;
php > echo "$a, $b";
11, 11
```

## 3.4 - Variáveis não inicializadas

Não é obrigatório inicializar variáveis no PHP, mas é uma ótima prática. Variáveis não inicializadas assumirão o valor padrão de seu tipo dependendo do contexto em que são usadas:

* **Booleanos**: false
* **Integers e floats**: 0
* **Strings**: string vazia
* **Arrays**: array vazia

## 3.5 - Escopo das variáveis

Nós podemos entender "escopo" como os limites da disponibilidade de uma variável no nosso código. De modo geral, todas as variáveis no PHP têm escopo **global**, menos nas funções definidas pelo usuário, onde todas as variáveis são limitadas ao escopo **local** da função.

Neste exemplo, as variáveis `$a` e `$b` possuem escopo **global** e estão disponíveis em qualquer ponto da sessão, mas não são visíveis no interior da função soma, o que gera os erros de variável indefinida:

```
php > $a=1; $b=2;
php > function soma() { return $a + $b; }
php > echo $a + $b;
3
php > echo soma();
PHP Notice:  Undefined variable: a in php shell code on line 1
PHP Notice:  Undefined variable: b in php shell code on line 1
0
```

Para utilizarmos variáveis globais dentro de funções, elas precisam ser declaradas com a palavra chave `global`:

```
php > $a=1; $b=2;
php > function soma() { global $a, $b; return $a + $b; }
php > echo soma();
3
```

Outra forma de acessar as variáveis do escopo global é através da variável especial do PHP `$GLOBALS`, uma array associativa com os nomes das variáveis globais nos índices e seus respectivos conteúdos nos valores:

```
php > $a=1; $b=2;
php > function menos() { return $GLOBALS['b'] - $GLOBALS['a']; }
php > echo menos();
1
```

> **Nota:** A variável `$GLOBALS` existe em qualquer escopo e é o que se chama de **superglobal** no PHP -- variáveis internas do PHP e constantes que estão sempre disponíveis em todos os escopos.

Ainda nas funções, nós podemos declarar outro tipo variável local: as variáveis estáticas. Uma variável estática só existe no escopo local da função, mas não perde seu valor quando o programa sai desse escopo. Por exemplo:

```
php > function contador() { $a = 0; echo $a; $a++; }
php > contador();
0
php > contador();
0
php > contador();
0
...
```

Toda vez que a função `contador()` é chamada, o valor de `$a` torna-se `0` novamente e, portanto, o incremento (`$a++`) não acontece. Para que o valor atual não se perca, é possível declarar a variável `$a` como estática com o modificador `static`:

```
php > function contador() { static $a = 0; echo $a; $a++; }
php > contador();
0
php > contador();
1
php > contador();
2
php > contador();
3
...
```

> **Nota:** Os modificadores `global` e `static` implementam uma **referência** às variáveis fora do escopo local da função, ou seja, são como *apelidos* das variáveis originais.

## 3.6 - Variáveis variáveis

As variáveis também poder ser definidas e usadas dinamicamente no PHP, ou seja, seus valores podem ser usados como nomes de outras variáveis. Para isso, a atribuição deve ser feita com dois cifrões (`$$`) antes do nome da variável cujo valor queremos usar. Por exemplo:

```
php > $str= 'Olá';
php > $$str = 'mundo';
php > echo "$str, ${$str}!";
Olá, mundo!
```

No caso, `${$str}` é o mesmo que:

```
echo $Olá;
mundo
```

No caso das arrays, é preciso especificar o que queremos. Veja o exemplo:

```
php > $frutas = ['banana', 'laranja', 'abacate'];
php > $$frutas[1] = 'pera';
PHP Notice:  Array to string conversion in php shell code on line 1
```

O interpretador não teria como saber se queremos usar o valor em `$frutas[1]` como o nome da nova variável, ou se estamos tentando usar toda a array `$frutas` como nome (daí o erro de conversão de array para string) para, dessa nova variável, obter o índice `1`. 

Para resolver essa ambiguidade, nós podemos escrever:

```
php > ${$frutas[1]} = 'pera';
php > echo ${$frutas[1]};
pera
```

O que equivale a...

```
php > echo $laranja;
pera
```

## 3.7 - Tipos de dados

Quanto aos tipos de dados que uma variável pode conter, nós já vimos que o PHP os detecta automaticamente a partir do contexto em que as variáveis aparecem. Também vimos que seus tipos podem ser do tipo **booleano**, **inteiro**, **flutuante** e **string**. No caso das arrays, os tipos dizem respeito aos seus elementos, ou seja, cada um de seus elementos pode ser de qualquer um desses quatro tipos.

### Tipo booleano

Um dado do tipo booleano expressa a verdade de uma expressão, que pode ser verdadeira (`true`) ou falsa (`false`). Para expressar um valor booleano de forma literal, nós usamos as constantes `TRUE` e `FALSE`, que podem ser escritas indiferentemente em maiúsculas ou minúsculas.

```
$a = true;
$b = True;
$c = TRUE;
```

Embora geralmente desnecessário, também podemos converter valores em booleanos usando os modificadores `(bool)` ou `(boolean)`. Neste caso, todos os valores abaixo seriam considerados `false`:

* O próprio booleano `FALSE`
* Os inteiros `0` e `-0`
* Os flutuantes `0.0` e `-0.0`
* Strings vazias e a string `0`
* Uma array sem elementos
* O tipo especial `NULL` e variáveis indefinidas
* Objetos `SimpleXML` criados a partir de tags vazias

Todos os demais valores são considerados `TRUE`.

> **Nota:** Os modificadores de conversão de tipos, sejam eles quais forem, são utilizados imediatamente antes do acesso ao nome das variáveis:

```
php > $a = 2;
php > echo (bool)$a;
1
```

### Tipo inteiro

Inteiros são os números naturais e seus simétricos negativos. Eles podem ser especificados nas bases 10 (decimal), 16 (hexadecimal), 8 (octal) ou 2 (binário):

* **Base 16**: precedido por `0x`
* **Base 8**: precedido por `0`
* **Base 2**: precedido por `0b`

> **Nota:** A partir do PHP 7.4.0, inteiros literais podem conter caracteres de sublinhado (`_`) entre os dígitos para facilitar a legibilidade.

O tamanho da integer depende da plataforma, mas o limite usual é de cerca de 2.14 bilhões positivos ou negativos o que corresponde a 32 bits com sinal (o PHP não suporta inteiros sem sinal).

Nós podemos ver o tamanho máximo dos inteiros na nossa plataforma com a constante `PHP_INT_MAX`:

```
php > echo PHP_INT_MAX;
9223372036854775807
```

Também podemos ver quantos bytes são alocados para uma integer com a constante `PHP_INT_SIZE`:

```
php > echo PHP_INT_SIZE;
8
```

Para converter explicitamente um valor em inteiro, nós podemos usar os modificadores `(int)` ou `(integer)`. Geralmente, isso é desnecessário, já que os valores serão automaticamente convertidos se um operador, uma função ou uma estrutura de controle requererem um inteiro como argumento.

Na conversão...

* `FALSE` é convertido para `0` e `TRUE` para `1`.
* Valores flutuantes retornam apenas a parte inteira.
* Valores flutuantes além dos limites dos inteiros o resultado será indefinido.
* Se a string não contiver `.`, `e`, `E`, e o valor estiver nos limites de `PHP_INT_MAX`, ela será convertida para inteiro.
* O tipo especial `NULL` sempre será convertido para `0`.

### Números de ponto flutuante

Números de ponto flutuante (também chamados de "floats", "doubles" ou "números reais") são aqueles que possuem casas decimais e podem ser expressos no PHP como:

```
$f = 1.234;
$f = 1.2e3;
$f = 5E-10;
$f = 1_234.567; // (PHP >= 7.4.0)
```

O valor máximo de um número flutuante pode ser visto com a constante `PHP_FLOAT_MAX` e é dependente da plataforma:

```
php > echo PHP_FLOAT_MAX;
1.7976931348623E+308
```

> **Nota:** Números de ponto flutuante possuem precisão limitada e não podemos confiar em seus resultados até o último dígito.

Eventualmente, algumas operações numéricas podem resultar em um valor representado pela constante `NAN`, que expressa um valor indefinido ou incapaz de ser representado em cálculos envolvendo pontos flutuantes, o que pode ser verificado com a função `is_nan()`.

### Strings

Uma string é uma cadeia de caracteres, onde cada caractere tem o tamanho de um byte. Isso significa que o PHP suporta apenas um conjunto de 256 caracteres e, portanto, não oferece suporte a caracteres Unicode nativamente. 

Todos os valores sempre serão tratados como strings no contexto de uma expressão que precise de uma string, como em um `echo` ou um `print`, por exemplo, ou quando o valor de uma variável for comparado com uma string. Mesmo assim, podemos converter valores em strings com o modificador `(string)` ou a função `strval()`.

Na conversão...

* `TRUE` é convertido para `"1"` e `FALSE` é convertido para `""`.
* Inteiros e flutuantes são convertidos para a representação textual de seus valores.
* Arrays são sempre convertidas para a string `"Array"`.
* `NULL` é sempre convertida para uma string vazia.

Uma string literal pode ser expressa de 4 formas diferentes:

* Entre aspas simples
* Entre aspas duplas
* Por um *heredoc* (equivale a aspas duplas)
* Por um *nowdoc* (equivale a aspas simples)

**Aspas simples**

A forma mais simples de representar uma string é com as aspas simples. Para escapar (tornar literais) as aspas simples em uma string entre aspas simples, nós utilizamos o caractere `\` (barra invertida). Para expressar uma barra invertida literal, nós escrevemos `\\`. Qualquer outra ocorrência da barra invertida será tratada como uma representação literal, ou seja, caracteres de controle (como `\n` ou `\r`), por exemplo, serão tratados literalmente:

```
php > echo 'banana\n\'laranja\'\nabacate\\';
banana\n'laranja'\nabacate\
```

**Aspas duplas**

É possível escapar aspas duplas e a barra invertida, mas os caracteres de controle serão interpretados:

```
php > echo "banana\n\"laranja\"\nabacate\\";
banana
"laranja"
abacate\
```

Além disso, as variáveis que aparecerem numa string entre aspas duplas serão expandidas, a menos que o `$` de seus nomes sejam escapados:

```
php > $a = 'laranja';
php > echo "banana $a";
banana laranja
php > echo "banana \$a";
banana $a
```

**Heredoc**

O *heredoc* tem o mesmo efeito das aspas duplas envolvendo uma string em várias linhas:


```
php > $frutas = "
php " banana
php " laranja
php " abacate
php " ";
php > echo $frutas;

banana
laranja
abacate
```

Com heredoc:

```
php > $bichos = <<<FIM
<<< > leão
<<< > zebra
<<< > elefante
<<< > FIM;
php > echo $bichos;
leão
zebra
elefante
```

**Nowdoc**

Um *nowdoc* tem o mesmo efeito das aspas simples envolvendo uma string em várias linhas. A diferença para as heredocs é que o identificador aparece entre aspas simples:


```
php > $carros = <<<'FIM'
<<< > kia
<<< > peugeot
<<< > ferrari
<<< > FIM;
php > echo $carros;
kia
peugeot
ferrari
```

## 3.8 - Arrays

Sem nos aprofundarmos muitos em detalhes sobre tipos de dados que fogem dos nossos propósitos, nós podemos definir arrays como uma estrutura de dados que associa valores a índices. As arrays podem ser criadas usando o construtor de linguagem `array()`, onde são especificados pares de índices e valores:

```
$arr = array(
  'nome'  => 'João',
  'idade' => 23,
  'nota'  => 7.5
);
```

> **Nota:** A vírgula depois da especificação **do último elemento** não é obrigatória e deve ser preferencialmente dispensada quando a instrução é dada em uma linha.

A partir do PHP 5.4, também podemos utilizar a sintaxe abreviada `[ ]`:

```
$arr = [
  'nome'  => 'João',
  'idade' => 23,
  'nota'  => 7.5
];
```

De qualquer forma, a chave pode ser uma string ou um inteiro, e o valor pode conter valores de qualquer tipo. Além disso, as seguintes transformações também vão ocorrer nos índices:

* Strings expressando valores inteiros decimais válidos e sem sinal serão convertidas para inteiro.
* Números flutuantes serão truncados e convertidos para inteiros.
* Valores booleanos serão convertidos para inteiros.
* `NULL` será convertida para uma string vazia.

Se vários elementos forem declarados com o mesmo índice, apenas o último será usado e os demais serão sobrescritos:

```
php > $arr = [
php > 1 => 'a',
php > '1' => 'b',
php > 1.5 => 'c',
php > true => 'd'
php > ];
php > print_r($arr);
Array
(
    [1] => d
)
```

A especificação de um índice é opcional no PHP, e ainda é possível definir o índice de alguns elementos e deixar os demais indefinidos. Quando os índices não são especificados, o PHP irá utilizar um incremento do último maior valor inteiro usado anteriormente.

```
php > $arr = ['banana', 'laranja', 'pera'];
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => pera
)
```

> **Nota:** Arrays e objetos não podem ser usados como índices!

O PHP também permite o uso de arrays como valores de elementos de uma array, e isso é o que nos permite trabalhar com arrays multidimensionais:

```
php > $alunos = [
php > ['nome' => 'João', 'idade' => 23, 'nota' => 7.5],
php > ['nome' => 'Maria', 'idade' => 25, 'nota' => 8.0],
php > ['nome' => 'Pedro', 'idade' => 22, 'nota' => 7.0]
php > ];
php > print_r($alunos);
Array
(
    [0] => Array
        (
            [nome] => João
            [idade] => 23
            [nota] => 7.5
        )

    [1] => Array
        (
            [nome] => Maria
            [idade] => 25
            [nota] => 8
        )

    [2] => Array
        (
            [nome] => Pedro
            [idade] => 22
            [nota] => 7
        )

)
```

Os valores de uma array podem ser modificados pela especificação do índice do elemento que queremos alterar:

```
php > $arr = ['banana', 'laranja', 'pera'];
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => pera
)
php > $arr[2] = 'abacate';
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => abacate
)
```

Também podemos incluir novos elementos em uma array sem especificar um índice:

```
php > $arr[3] = 'pera'; // incluindo elemento com índice
php > $arr[] = 'limão'; // incluindo elemento sem índice
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [2] => abacate
    [3] => pera
    [4] => limão
)
```

Para remover um elemento da array, nós usamos a função `unset()`:

```
php > unset($arr[2]);
php > print_r($arr);
Array
(
    [0] => banana
    [1] => laranja
    [3] => pera
    [4] => limão
)
```

## 3.9 - Constantes

Constantes são apenas identificadores de valores e, como o nome sugere, esses valores não podem mudar durante a execução do script. Por convenção, os identificadores das constantes são sempre expressos em maiúsculas.

Para criar uma constante, nós utilizamos a função `define()`:

```
define('MINHA_CONSTANTE', 'qualquer valor');
```

Independente do escopo, as constantes são sempre globais, ou seja, podem ser acessadas de qualquer lugar no script:

```
php > define('MINHA_CONSTANTE', 10);
php > function soma() { return MINHA_CONSTANTE + 5; }
php > echo soma();
15
```
