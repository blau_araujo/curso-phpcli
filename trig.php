<?php

/*
function meus_erros ($num, $msg, $arq, $linha) {
    echo "Nível   : $num\n";
    echo "Mensagem: $msg\n";
    echo "Arquivo : $arq\n";
    echo "Linha   : $linha\n";
    exit;
}

set_error_handler('meus_erros');
*/

$valor = readline("Digite um número menor do que 10: ");

if ($valor >= 10) {
    trigger_error("O número deve ser menor do que 10!");
}
