<?php

$data = 'id|nome|idade|email
001|João da Silva|23|jsilva@imail.com
002|Maria das Couves|19|mcouves@jmail.com
003|Antônio Pedro|26|apedro@qmail.com';



function cadastro($dt) {

    $linhas = explode("\n", $dt);

    $ncampos = explode("|", trim($linhas[0]));

    array_shift($linhas);

    foreach ($linhas as $linha) {
        $campos = explode("|", $linha);
        yield array_combine($ncampos, $campos);
    }
}


foreach (cadastro($data) as $v) {
    print_r($v);
}
