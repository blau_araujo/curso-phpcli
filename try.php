<?php

try {
    $f = 'banana.txt';

    if ( !file_exists($f) ) {
        throw new Exception("Arquivo $f não existe!", 30);
    }
}

catch (Exception $erro) {
    echo $erro->getMessage()."\n";
    echo $erro->getCode()."\n";
    echo $erro->getFile()."\n";
    echo $erro->getLine()."\n";
}
